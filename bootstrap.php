<?php
    define('ROOT_PATH', __DIR__);

    include('core/loader.php');

    (Dotenv\Dotenv::create(__DIR__))->load();

    include('core/config.php');
