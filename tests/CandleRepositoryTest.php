<?php


use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class CandleRepositoryTest extends TestCase
{
    protected $em;
    public function setUp()
    {
        $this->em = DB::getInstance()->getEntityManager();
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    /**
     * @throws DBALException
     */
    public function testGetTotalCandlesByPair(): void
    {
        $pairRepository = $this->em->getRepository(Pair::class);
        $this->assertInstanceOf(EntityRepository::class, $pairRepository);

        /**
         * @var Pair $pair
         */
        $pair = $pairRepository->findOneBy([]);

        if(null !== $pair){
            /**
             * @var CandleRepository $candleRepository
             */
            $candleRepository = $this->em->getRepository(Candle::class);
            $this->assertInstanceOf(CandleRepository::class, $candleRepository);
            $result = $candleRepository->getTotalCandlesByPair($pair);
            $this->assertGreaterThan(0, $result);
        }
    }
}
