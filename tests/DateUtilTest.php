<?php


use PHPUnit\Framework\TestCase;

class DateUtilTest extends AbstractSimpleDatabaseTest
{
    /**
     * @param $class
     */
    public function setClassName($class): void
    {
        parent::setClassName(DateUtil::class);
    }

    /**
     * @return DateUtil
     */
    public function getInstance()
    {
        return parent::getInstance();
    }

    public function testDateConvert()
    {
        $instance = $this->getInstance();
        $testMatrix = [
            ['2020-01-01 01:00:00', true, '01/01/2020 às 01:00:00'],
            ['2020-01-01 01:00:00', false, '01/01/2020'],
            ['20/01/2020', true, '2020-01-20'],
            ['20/01/2020', false, '2020-01-20']
        ];

        foreach ($testMatrix as $testcase) {
            $result = $instance::dateConvert($testcase[0], $testcase[1]);
            $this->assertEquals($testcase[2], $result);
        }
    }

    public function testNormalize()
    {
        $instance = $this->getInstance();
        $testMatrix = [
            ['2020-01-01 23:00:00', 1, '2020-01-02 00:00:00'],
            ['2020-01-02 00:00:00', -1, '2020-01-01 23:00:00']
        ];

        foreach ($testMatrix as $testcase) {
            $result = $instance::normalize(new DateTime($testcase[0]), $testcase[1]);
            $this->assertEquals($testcase[2], $result->format('Y-m-d H:i:s'));
        }
    }

    /**
     * @throws Exception
     */
    public function testSumDaysToDate(): void
    {
        $testMatrix = [
            ['2020-06-23', -11, '2020-06-08'],
            ['2020-06-26', -5, '2020-06-19'],
            ['2020-06-24', -5, '2020-06-17'],
            ['2020-06-23', -2, '2020-06-19'],
            ['2020-06-26', -1, '2020-06-25'],
            ['2020-06-26', 1, '2020-06-29'],
            ['2020-06-26', 6, '2020-07-06'],
            ['2020-06-25', 1, '2020-06-26'],
            ['2020-06-25', 2, '2020-06-29'],
            ['2020-06-22', 5, '2020-06-29']
        ];
        $instance = $this->getInstance();

        foreach ($testMatrix as $index => $testcase) {
            $result = $instance::sumDaysToDateString(new DateTime($testcase[0]), $testcase[1]);
            $this->assertInstanceOf(DateTimeInterface::class, $result);
            $this->assertSame($testcase[2], $result->format('Y-m-d'), "Teste " . ($index + 1) . " falhou");
        }
    }
}
