<?php


use Doctrine\ORM\OptimisticLockException;
use PHPUnit\Framework\TestCase;

class EntryServiceTest extends AbstractSimpleDatabaseTest
{
    public function setClassName($class): void
    {
        parent::setClassName(EntryService::class);
    }

    public function setClassArgs(...$args): void
    {
        parent::setClassArgs($this->getEm(), $this->getEm()->getRepository(Entry::class));
    }

    /**
     * @return EntryService
     */
    public function getInstance()
    {
        return parent::getInstance();
    }

    public function testGenerateEntryByCandleGroupAndBacktest()
    {
        $this->assertTrue(true);
    }

    public function testCheckEntryResult()
    {
        $this->assertTrue(true);
    }

    /**
     * @throws OptimisticLockException
     */
    public function testConfirmEntries()
    {
        $this->getEm()->beginTransaction();

        $pair = new Pair();
        $pair->currencyA = 'FOO';
        $pair->currencyB = 'BAR';
        $pair->interval = 30;
        $this->getEm()->persist($pair);
        $this->getEm()->flush();

        $backtest = new Backtest();
        $backtest->setPair($pair);
        $backtest->setUpdated('2020-01-01');
        $backtest->time = '00:00:00';
        $backtest->interval = 5;
        $backtest->tolerance = 0.6;
        $this->getEm()->persist($backtest);
        $this->getEm()->flush();

        $entry = new Entry();
        $entry->setBacktest($backtest);
        $entry->setPair($pair);
        $entry->date = $backtest->updatedAt;
        $entry->time = $backtest->time;
        $entry->interval = $backtest->interval;
        $entry->tolerance = $backtest->tolerance;
        $entry->status = EntryStatusEnum::WAITING;
        $entry->operation = OperationEnum::CALL;
        $entry->points = 0;
        $this->getInstance()->save($entry);

        $candle = new Candle();
        $candle->pair = $pair;
        $candle->setDate($entry->date);
        $candle->time = $entry->time;
        $candle->interval = $pair->interval;
        $candle->result = OperationEnum::CALL;
        $candle->points = 10;
        $candle->open = 0;
        $candle->close = 1;
        $candle->min = 0;
        $candle->max = 1;
        $candle->volume = 50;
        $this->getEm()->persist($candle);
        $this->getEm()->flush();

        $totalConfirmed = $this->getInstance()->confirmEntries();
        $this->assertSame(1, $totalConfirmed);

        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->getEm()->getRepository(Entry::class);
        /**
         * @var Entry $entry
         */
        $entry = $entryRepository->find($entry->getIdEntry());
        $this->assertNotNull($entry);

        $this->assertSame(EntryStatusEnum::SUCCESS, $entry->status);
        $this->assertSame(10, $entry->points);
        $this->assertSame(1, $entry->getBacktest()->totalRights);
        $this->assertSame($entry->date . ' '. $entry->time, $entry->getBacktest()->updatedAt);
        $this->assertSame(1, $entry->getBacktest()->sequenceRights);

        $totalConfirmed = $this->getInstance()->confirmEntries();
        $this->assertSame(0, $totalConfirmed);

        $this->getEm()->rollback();
    }

    public function testBuildEntryByCandleAndBacktest()
    {
        $this->assertTrue(true);
    }

    public function testSave()
    {
        $this->assertTrue(true);
    }

    public function testGetUnconfirmedWatchedEntryAsCsvByDate()
    {
        $this->assertTrue(true);
    }

    public function testGetGroupedWatchedEntriesByDate()
    {
        $this->assertTrue(true);
    }

    public function testCanGenerateEntry()
    {
        $this->assertTrue(true);
    }
}
