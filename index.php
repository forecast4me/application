<?php
include('bootstrap.php');

if(!ApiService::checkApplicationCredentials()){
    echo 'Aplicação não autorizada';
    exit();
}

$app = new App();
$app->run();
