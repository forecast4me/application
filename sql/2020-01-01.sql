--
-- Table structure for table `pair`
-- Created with MySQL Version 10.1.28
--

CREATE TABLE `pair` (
  `idpair` int(11) NOT NULL AUTO_INCREMENT,
  `currency_a` varchar(50) NOT NULL,
  `currency_b` varchar(50) NOT NULL,
  `_interval` int(11) NOT NULL,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `last_line_read` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpair`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `backtest`
-- Created with MySQL Version 10.1.28
--

CREATE TABLE `backtest` (
  `idbacktest` int(11) NOT NULL AUTO_INCREMENT,
  `pair_idpair` int(11) NOT NULL DEFAULT '0',
  `_time` time NOT NULL,
  `_interval` int(11) NOT NULL,
  `tolerance` decimal(10,6) NOT NULL,
  `errors_count` int(11) DEFAULT '0',
  `rights_count` int(11) DEFAULT '0',
  `sequence_errors` int(11) DEFAULT '0',
  `sequence_rights` int(11) DEFAULT '0',
  `total_errors` int(11) DEFAULT '0',
  `total_rights` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbacktest`),
  KEY `FK_backtest_result_pair` (`pair_idpair`),
  KEY `_time` (`_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `candle`
-- Created with MySQL Version 10.1.28
--

CREATE TABLE `candle` (
  `idcandle` int(11) NOT NULL AUTO_INCREMENT,
  `pair_idpair` int(11) NOT NULL,
  `_date` date NOT NULL,
  `_time` time NOT NULL,
  `_interval` int(11) NOT NULL,
  `_open` decimal(10,5) NOT NULL,
  `_max` decimal(10,5) NOT NULL,
  `_min` decimal(10,5) NOT NULL,
  `_close` decimal(10,5) NOT NULL,
  `volume` decimal(10,5) NOT NULL,
  `points` decimal(10,5) NOT NULL,
  `result` tinyint(1) NOT NULL,
  PRIMARY KEY (`idcandle`),
  KEY `pair_idpair` (`pair_idpair`),
  KEY `_time` (`_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `entry`
-- Created with MySQL Version 10.1.28
--

CREATE TABLE `entry` (
  `identry` int(11) NOT NULL AUTO_INCREMENT,
  `pair_idpair` int(11) NOT NULL,
  `backtest_idbacktest` int(11) NOT NULL,
  `operation` int(11) NOT NULL COMMENT '0 - PUT; 1 - CALL',
  `_date` date NOT NULL,
  `_time` time NOT NULL,
  `_interval` int(11) NOT NULL,
  `tolerance` decimal(10,5) NOT NULL,
  `points` decimal(10,5) NOT NULL,
  `_status` int(11) NOT NULL COMMENT '0 - FAIL; 1 - SUCCESS; 2 - WAITING',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `watched` boolean default 0 null,
  PRIMARY KEY (`identry`),
  KEY `FK_entry_pair` (`pair_idpair`),
  KEY `FK_entry_backtest` (`backtest_idbacktest`),
  FOREIGN KEY (`backtest_idbacktest`) REFERENCES backtest(`idbacktest`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

