<?php


use Doctrine\ORM\EntityRepository;

class BacktestRepository extends EntityRepository
{
    /**
     * @param Pair $pair
     * @param      $time
     * @param      $interval
     * @param      $tolerance
     * @return Backtest | object | null
     */
    public function findLastBacktestByPairTimeIntervalTolerance($pair, $time, $interval, $tolerance)
    {
        return $this->findOneBy([
            'pair' => $pair,
            'time' => $time,
            'interval' => $interval,
            'tolerance' => $tolerance
        ]);
    }
}
