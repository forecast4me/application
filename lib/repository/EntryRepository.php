<?php

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class EntryRepository extends EntityRepository
{
    /**
     * @param Pair $pair
     * @param $date
     * @param $time
     * @param $interval
     * @param $tolerance
     * @return Entry | object
     */
    public function findByPairDateTimeInterval($pair, $date, $time, $interval, $tolerance)
    {
        return $this->findOneBy([
            'pair' => $pair,
            'date' => $date,
            'time' => $time,
            'interval' => $interval,
            'tolerance' => $tolerance
        ]);
    }

    /**
     * @param Pair $pair
     * @param $interval
     * @param $tolerance
     * @return Entry[]
     */
    public function findByLatestEntriesByPairIntervalTolerance($pair, $interval, $tolerance): ?array
    {
        $sql = 'SELECT _date as date FROM entry 
                    WHERE 
                    pair_idpair = :idpair 
                    AND _interval = :interval 
                    AND tolerance = :tolerance 
                ORDER BY _date DESC LIMIT 1';
        $db = DB::getInstance()->getPDO();

        $stmt = $db->prepare($sql);

        $idpair = $pair->getIdPair();
        $stmt->bindParam(':idpair', $idpair);
        $stmt->bindParam(':interval', $interval);
        $stmt->bindParam(':tolerance', $tolerance);
        $stmt->execute();

        $resultSet = $stmt->fetchAll();

        if (!count($resultSet)) {
            return null;
        }

        $result = $resultSet[0];
        $lastEntryDate = $result['date'];

        $dql = 'SELECT e FROM Entry e WHERE e.date = :date AND e.pair = :pair AND e.tolerance = :tolerance AND e.interval = :interval';
        $stmt = DB::getInstance()->getEntityManager()->createQuery($dql);
        $stmt->setParameters([
            'date' => $lastEntryDate,
            'pair' => $pair,
            'tolerance' => $tolerance,
            'interval' => $interval
        ]);

        return $stmt->getResult();
    }

    /**
     * @param null | DateTimeInterface $from
     * @param null | DateTimeInterface $to
     * @param bool $watchedOnly
     * @return array | Entry[]
     */
    public function findByDate(?DateTimeInterface $from = null, ?DateTimeInterface $to = null, $watchedOnly = false): array
    {
        if (null === $from) {
            $from = new DateTime();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')->from(Entry::class, 'e');

        $where = [];
        if (null !== $from && null !== $to) {
            $where[] = 'e.date BETWEEN :dateFrom AND :dateTo';
            $parameters['dateFrom'] = $from;
            $parameters['dateTo'] = $to;
        } else if (null !== $from && null === $to) {
            $where[] = 'e.date >= :dateFrom';
            $parameters['dateFrom'] = $from;
        } else {
            $where[] = 'e.date <= :dateTo';
            $parameters['dateTo'] = $to;
        }

        if ($watchedOnly) {
            $where[] = 'e.watched = :watched';
            $parameters['watched'] = $watchedOnly;
        }

        return $qb
            ->where(implode(' AND ', $where))
            ->setParameters($parameters)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_OBJECT);
    }

    /**
     * @param Entry $entry
     */
    public function save($entry): void
    {
        $this->_em->persist($entry);
    }

    /**
     * @param Pair $pair
     * @param $timeKey
     * @param $interval
     * @param $tolerance
     * @return Entry | null
     */
    public function findByLatestEntryByPairTimekeyIntervalTolerance(Pair $pair, $timeKey, $interval, $tolerance): ?\Entry
    {
        return $this->findOneBy([
            'pair' => $pair,
            'time' => $timeKey,
            'interval' => $interval,
            'tolerance' => $tolerance
        ], [
            'date' => 'DESC'
        ]);
    }
}
