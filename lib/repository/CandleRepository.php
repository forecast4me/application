<?php

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CandleRepository extends EntityRepository
{
    /**
     * Find all candles since the last backtest, including the ones needed to keep doing backtest
     * @param $pair
     * @param $interval
     * @param string | DateTimeInterface $lastBacktestDate
     * @param string $time
     * @return array
     * @throws Exception
     */
    public function findCandlesForBacktest($pair, $interval, $lastBacktestDate, $time)
    {
        if ($lastBacktestDate instanceof DateTimeInterface) {
            $lastBacktestDate = $lastBacktestDate->format('Y-m-d') . ' ' . $time;
        }

        $dql = 'SELECT c FROM Candle c WHERE
                        c.pair = :pair AND
                        c.interval = :interval AND 
                        c.date <= :lastBacktestDate AND
                        c.time = :time ORDER BY c.date DESC';

        $stmt = $this->getEntityManager()->createQuery($dql);
        $stmt->setMaxResults($interval);
        $stmt->execute([
            'pair' => $pair,
            'interval' => $pair->interval,
            'time' => $time,
            'lastBacktestDate' => (new DateTime($lastBacktestDate))->format('Y-m-d')
        ]);

        $arrCandles = $stmt->getResult();
        $arrCandles = array_reverse($arrCandles);
        if (count($arrCandles)) {
            /**
             * @var Candle $candle
             */
            $candle = $arrCandles[0];
            /**
             * @var Candle $lastCandle
             */
            $lastCandle = array_pop($arrCandles);

            if (strtotime($lastCandle->getDateTimeString()) < strtotime($lastBacktestDate)) {
                return [];
            }

            $getCandlesSince = $candle->getDate()->format('Y-m-d');

            $dql = 'SELECT c
                    FROM Candle c 
                    WHERE
                        c.pair = :pair AND
                        c.interval = :interval AND 
                        c.date >= :date AND
                        c.time = :time';

            $stmt = $this->getEntityManager()->createQuery($dql);
            $stmt->setParameter('pair', $pair);
            $stmt->setParameter('interval', $pair->interval);
            $stmt->setParameter('time', $time);
            $stmt->setParameter('date', $getCandlesSince);
        } else {
            $dql = 'SELECT c
                    FROM Candle c 
                    WHERE
                        c.pair = :pair AND
                        c.interval = :interval AND 
                        c.time =  :time';

            $stmt = $this->getEntityManager()->createQuery($dql);
            $stmt->setParameter('pair', $pair);
            $stmt->setParameter('interval', $pair->interval);
            $stmt->setParameter('time', $time);
        }

        return $stmt->execute();
    }

    public function getTimeKeysByIdPair($idpair)
    {
        //Get all the times available
        $db = DB::getInstance()->getPDO();
        $sql = "SELECT _time FROM candle c WHERE c.pair_idpair = :pair_idpair GROUP BY _time";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':pair_idpair', $idpair);
        $stmt->execute();
        $resultset = $stmt->fetchAll();
        if (count($resultset)) {
            return array_map(function ($el) {
                return $el['_time'];
            }, $resultset);
        }

        return null;
    }

    /**
     * @param Pair $pair
     * @return int
     * @throws DBALException
     */
    public function getTotalCandlesByPair($pair): int
    {
        $sql = 'SELECT COUNT(*) as total FROM candle c WHERE c.pair_idpair = :idpair';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute([
            'idpair' => $pair->getIdpair()
        ]);
        $result = $stmt->fetchAll();

        return $result[0]['total'] ?? 0;
    }

    /**
     * @param Pair $pair
     * @param $timeKey
     * @param $interval
     * @return null | Candle
     */
    public function findByLatestCandleByPairTimekeyInterval(Pair $pair, $timeKey, $interval): ?\Candle
    {
        return $this->findOneBy([
            'pair' => $pair,
            'time' => $timeKey,
            'interval' => $interval
        ], [
            'date' => 'DESC'
        ]);
    }
}
