<?php
/**
 * @var $timezoneDiff
 */

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    include('lib/includes/head.php');
    ?>
</head>
<body>
<div class="container">
    <?php include('lib/view/CallTrade/menu-top.part.php') ?>

    <div class="row">
        <div class="col-12">
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="timezoneDiff">UTC</label>
                    <input type="number" class="form-control" id="timezoneDiff" name="timezoneDiff" value="<?=$timezoneDiff?>" aria-describedby="timezoneDiffHelp" placeholder="-3 (Padrão UTC para BR)">
                    <small id="timezoneDiffHelp" class="form-text text-muted">Compensação de fuso horário.</small>
                </div>
                <button type="submit" class="btn btn-success">Salvar preferências</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
