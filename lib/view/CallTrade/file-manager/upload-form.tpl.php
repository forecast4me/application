<?php
/**
 * @var Pair[] $pairs
 * @var $idpair
 * @var $days
 * @var $tolerance
 * @var Entry[] $entries
 */

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    include('lib/includes/head.php');
    ?>
    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker();
        });
    </script>
</head>
<body>
<div class="container">
    <?php include('lib/view/CallTrade/menu-top.part.php') ?>

    <div class="row">
        <div class="col-12">
            <h3 class="text-center">Upload de arquivos</h3>
            <form class="form-horizontal backtest-filter-bar" id="filtro"
                  action="<?= URL::make('CallTrade', 'uploadFiles') ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-12">
                        <div class="row" style="display: flex; margin-bottom: 15px">
                            <div class="col-4 offset-4">
                                <label for="file" class="col-xs-12">
                                    Arquivos CSV:
                                </label>
                                <input type="file" name="file[]" multiple required>
                            </div>
                        </div>
                        <div class="row" style="display: flex; margin-bottom: 15px">
                            <div class="col-4 offset-4">
                                <label for="file" class="col-12">
                                    Limitar data de importação:
                                </label>
                                <input type="text" name="dateLimitToImport" class="datepicker form-control"
                                       value="<?= (new DateTime('now'))->format('d/m/Y') ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4 offset-4">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
