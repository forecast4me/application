<?php
    /**
     * @var Pair[] $pairs
     * @var $idpair
     * @var $days
     * @var $tolerance
     * @var $minRightsRatio
     */
?>

<script>
    $(document).ready(function(){
        $('.chosen').chosen({
            no_results_text: "Sem resultados para",
            placeholder_text_single: "Selecione uma opção",
            placeholder_text_multiple: "Selecione as opções"
        });
    });
</script>
<h3>Processamento de backtest</h3>
<form class="form-horizontal backtest-filter-bar" id="filtro" action="<?=URL::make("CallTrade", "doBacktest")?>" method="post">
    <div class="row">
        <div class="col-12">
            <div class="row" style="display: flex">
                <div class="col-4">
                    <label for="idpair" class="col-12">
                        Moeda:
                    </label>
                    <select name="idpair" id="idpair" class="form-control chosen" required>
                        <option value="">Selecione...</option>
                        <?php
                            if(count($pairs)):
                                foreach($pairs as $pair):
                                    ?>
                                    <option value="<?=$pair->getIdpair()?>" <?php echo ($pair->getIdpair() === $idpair) ? 'selected' : null?>><?=$pair->getPairName()?></option>
                                <?php
                                endforeach;
                            endif;
                        ?>
                    </select>
                </div>
                <div class="col-2">
                    <label for="tolerance" class="col-xs-12" title="Número de dias de amostragem para o backtest">
                        Intervalo em dias:
                    </label>
                    <input type="number" maxlength="2" name="days" class="form-control" value="<?php echo $days?>">
                </div>
                <div class="col-2">
                    <label for="tolerance" class="col-xs-12" title="Variação de padrão aceita para gerar entrada para o dia seguinte">
                        Porcentagem:
                    </label>
                    <input type="number" maxlength="3" name="tolerance" class="form-control" value="<?php echo $tolerance?>">
                </div>
                <div class="col-2">
                    <label for="tolerance" class="col-xs-12" title="Filtra a porcentagem de acerto mínima">
                        Mínimo % acerto:
                    </label>
                    <input type="number" maxlength="3" name="minRightsRatio" class="form-control" value="<?php echo $minRightsRatio?>">
                </div>
                <div class="col-2 hidden-print" style="display: flex; align-items: flex-end">
                    <button type="submit" class="btn btn-success">Processar</button>
                </div>
            </div>
        </div>
    </div>
</form>
