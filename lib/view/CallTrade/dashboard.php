<?php
/**
 * @var Pair[] $pairs
 * @var $idpair
 * @var $days
 * @var $tolerance
 * @var Entry[] $entries
 * @var Backtest[] $backtests
 */

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    include('lib/includes/head.php');
    ?>
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.entries-component .table').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                },
                paging: false
            });

            $('.backtests-component .table').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                },
                paging: false,
                "order": [[1, 'desc']]
            });

            $(document).on('click', '.watch-entry', function(){
                let icon = $(this).find('i');

                $.ajax({
                    type: 'POST',
                    url: window['backend_url'] + 'CallTradeApi/watchEntry',
                    data: JSON.stringify({'entryId': $(this).data('id')}),
                    contentType: "application/json",
                    dataType: 'json'
                });

                if($(this).data('watched')) {
                    icon.removeClass('fa-star');
                    icon.addClass('fa-star-o');
                    $(this).data('watched', false);
                }else{
                    icon.removeClass('fa-star-o');
                    icon.addClass('fa-star');
                    $(this).data('watched', true);
                }
            })
        });
    </script>
</head>
<body>
<div class="container">
    <?php include('lib/view/CallTrade/menu-top.part.php') ?>

    <div class="row">
        <div class="col-12">
            <?php include('backtest-form.part.php') ?>

            <?php
            if (null !== $idpair):
                if (count($entries ?? [])):
                    ?>
                    <div class="content entries-component">
                        <h4>Entradas para o dia <?php echo EntryFormatter::getDate($entries[0]) ?></h4>
                        <table class="table table-stripped table-hover table-result data-table">
                            <thead>
                            <tr>
                                <th>Hora</th>
                                <th>Hora MT4</th>
                                <th>Direção</th>
                                <th>% acertos</th>
                                <th title="Pontuação">PTS</th>
                                <th title="Martingale">Martingale</th>
                                <th title="Máximo de erros consecutivos">MEC</th>
                                <th title="Máximo de acertos concecutivos">MAC</th>
                                <th title="Total de acertos">TA</th>
                                <th title="Total de erros">TE</th>
                                <th title="Monitorar">Monitorar</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($entries as $entry):
                                $datetime = new \DateTime($entry->time);
                                ?>
                                <tr>
                                    <td data-order="<?= $datetime->getTimestamp() ?>">
                                        <?php echo DateFormatter::parseTime(
                                            $entry->time,
                                            DateUtil::getTimezoneDiff()
                                        ) ?></td>
                                    <td data-order="<?= $datetime->getTimestamp() ?>">
                                        <?php echo DateFormatter::parseTime($entry->time) ?>
                                    </td>
                                    <td><?php echo EntryFormatter::getOperation($entry) ?></td>
                                    <td data-order="<?= $entry->getBacktest()->calcSuccessRatio() ?>">
                                        <?php echo BacktestFormatter::getSuccessRatio($entry->getBacktest()) ?>
                                    </td>
                                    <td data-order="<?= $entry->points ?>">
                                        <?php echo EntryFormatter::getPoints($entry) ?>
                                    </td>
                                    <td><?php echo $entry->getBacktest()->errorsCount ?? 0 ?></td>
                                    <td><?php echo $entry->getBacktest()->sequenceErrors ?? 0 ?></td>
                                    <td><?php echo $entry->getBacktest()->sequenceRights ?? 0 ?></td>
                                    <td><?php echo $entry->getBacktest()->totalRights ?? 0 ?></td>
                                    <td><?php echo $entry->getBacktest()->totalErrors ?? 0 ?></td>
                                    <td>
                                        <button class="btn btn-default btn-sm watch-entry"
                                                data-id="<?=$entry->getIdEntry()?>"
                                                data-watched="<?=$entry->getWatched() ? 'true': 'false'?>">
                                            <i class="fa <?=$entry->getWatched()?'fa-star':'fa-star-o'?>"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="11">
                                    Relatório gerado dia <?= date('d/m/Y') ?> às <?= date('H\hi') ?>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php
                else: ?>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4>Sem entradas geradas</h4>
                        <p>Isso ocorre pois nenhuma sequência de velas atendeu seus filtros. Tente uma nova combinação
                            de filtros para tentar gerar entradas</p>
                    </div>
                <?php
                endif;

                if (count($backtests)):
                    ?>
                    <div class="content backtests-component hidden-print">
                        <h4>Resultados das entradas</h4>
                        <table class="table table-stripped table-hover table-result data-table">
                            <thead>
                            <tr>
                                <th>Hora</th>
                                <th>Hora MT4</th>
                                <th>% acertos</th>
                                <th title="Máximo de erros consecutivos">MEC</th>
                                <th title="Máximo de acertos concecutivos">MAC</th>
                                <th title="Total de acertos">TA</th>
                                <th title="Total de erros">TE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($backtests as $backtest):
                                $datetime = new DateTime($backtest->time);
                                ?>
                                <tr>
                                    <td data-order="<?= $datetime->getTimestamp() ?>">
                                        <?php echo DateFormatter::parseTime(
                                                $backtest->time,
                                                DateUtil::getTimezoneDiff()
                                        ) ?>
                                    </td>
                                    <td data-order="<?= $datetime->getTimestamp() ?>">
                                        <?php echo DateFormatter::parseTime($backtest->time) ?>
                                    </td>
                                    <td data-order="<?= $backtest->calcSuccessRatio() ?>">
                                        <?php echo BacktestFormatter::getSuccessRatio($backtest) ?>
                                    </td>
                                    <td><?php echo $backtest->sequenceErrors ?? 0 ?></td>
                                    <td><?php echo $backtest->sequenceRights ?? 0 ?></td>
                                    <td><?php echo $backtest->totalRights ?? 0 ?></td>
                                    <td><?php echo $backtest->totalErrors ?? 0 ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7">Relatório gerado dia <?= date('d/m/Y') ?> às <?= date('H\hi') ?></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php
                endif;
            else:
                ?>
                <div class="content entries-component">
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4>Vamos começar?</h4>
                        <p>Selecione um par de moedas, informe seus filtros, e veja as entradas geradas</p>
                    </div>
                </div>
            <?php
            endif;
            ?>
        </div>
    </div>
</div>

</body>
</html>
