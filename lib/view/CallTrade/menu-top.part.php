<nav class="navbar navbar-expand-lg navbar-light bg-light hidden-print" style="margin-bottom: 20px">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo URL::make("CallTrade", "index") ?>">Backtest <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL::make("CallTrade", "entryWatcher") ?>">Monitor de entradas <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gerenciar pares
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo URL::make("CallTrade", "managePairs") ?>">Gerenciar</a>
                    <a class="dropdown-item" href="<?php echo URL::make("CallTrade", "uploadFiles") ?>">Cadastrar ou atualizar</a>
                </div>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Configurações <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Minha conta</a></li>
                    <li><a class="dropdown-item" href="<?php echo URL::make("CallTrade", "config") ?>">Sistema</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a class="dropdown-item" href="#">Sair</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
