import { Component } from '@angular/core';
import { EntryService } from "./services/entry.service";
import { EntryGroup } from "./models/entry-group";
import { Entry } from "./models/entry";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'entry-watcher';

  set entryList(value: Entry[]) {
    this._entryList = value;
  }

  get entryList() {
    return this._entryList;
  }

  get entryGroupList() {
    return this._entryGroupList;
  }

  set entryGroupList(value: EntryGroup[]) {
    this._entryGroupList = value;
  }

  private _entryGroupList: EntryGroup[] = [];
  private _entryList: Entry[] = [];

  public constructor(
    public entryService: EntryService
  ) {
    this.updateEntryGroupList();

    this.entryService.updateEntryGroupList$.subscribe(() => {
      this.updateEntryGroupList();
    });
    this.entryService.selectedEntryGroupChangeEvent$.subscribe((value: EntryGroup) => this.updateEntryList(value));
    this.updateEntryList(this.entryService.selectedEntryGroup);
  }

  public updateEntryGroupList(){
    this.entryService
      .getGroupedEntries()
      .subscribe((entryGroupList: EntryGroup[]) => {
        this.entryGroupList = entryGroupList
      });
  }

  public updateEntryList(entryGroup: EntryGroup) {
    if (entryGroup === null) return;

    this.entryService
      .getEntriesByDate(entryGroup.date, entryGroup.date)
      .subscribe((entryList) => { this.entryList = entryList; });
  }

  public getEntryGroupDownloadLink(entryGroup: EntryGroup) {
    return this.entryService.backendUrl + '/CallTrade/getUnconfirmedWatchedEntryAsCsvByDate/?date=' + entryGroup.date;
  }
}
