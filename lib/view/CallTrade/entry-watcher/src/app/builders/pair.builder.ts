import { BuilderInterface } from "./builder.interface";
import { Pair } from "../models/pair";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class PairBuilder implements BuilderInterface {
  build(item: any): Pair {
    return new Pair(
      item.idpair,
      item.currencyA,
      item.currencyB,
      item.interval
    )
  }
}
