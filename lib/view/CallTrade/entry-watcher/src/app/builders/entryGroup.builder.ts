import { BuilderInterface } from "./builder.interface";
import { EntryGroup } from "../models/entry-group";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class EntryGroupBuilder implements BuilderInterface {
  build(item: any): EntryGroup {
    return new EntryGroup(
      item.date,
      parseInt(item.rights),
      parseInt(item.errors),
      parseInt(item.waiting)
    );
  }
}
