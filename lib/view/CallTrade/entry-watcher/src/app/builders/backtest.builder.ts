import { BuilderInterface } from "./builder.interface";
import { Backtest } from "../models/backtest";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BacktestBuilder implements BuilderInterface{
  build(item: any): Backtest {
    return new Backtest(
      item.idbacktest,
      item.pair,
      item.interval,
      item.time,
      item.tolerance,
      item.errorsCount || 0,
      item.rightsCount || 0,
      item.totalErrors || 0,
      item.totalRights || 0,
      item.createdAt,
      item.updatedAt
    );
  }
}
