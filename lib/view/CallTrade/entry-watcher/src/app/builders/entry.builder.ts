import { BuilderInterface } from "./builder.interface";
import { PairBuilder } from "./pair.builder";
import { BacktestBuilder } from "./backtest.builder";
import { Entry } from "../models/entry";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class EntryBuilder implements BuilderInterface{
  constructor(
    private pairBuilder: PairBuilder,
    private backtestBuilder: BacktestBuilder
  ) {
  }
  build(item: any): Entry {
    return new Entry(
      item.identry,
      this.pairBuilder.build(item.pair),
      this.backtestBuilder.build(item.backtest),
      item.date,
      item.time,
      item.operation,
      item.interval,
      item.tolerance,
      item.points,
      item.status
    );
  }
}
