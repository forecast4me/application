import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { EntryGroup } from "../models/entry-group";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Entry } from "../models/entry";
import { DatePipe } from "@angular/common";
import { EntryGroupBuilder } from "../builders/entryGroup.builder";
import { EntryBuilder } from "../builders/entry.builder";

@Injectable({
  providedIn: 'root'
})
export class EntryService {
  public backendUrl: string = window['backend_url'] || environment.backend_url;
  get updateEntryGroupList$(): EventEmitter<any> {
    return this._updateEntryGroupList$;
  }

  set updateEntryGroupList$(value: EventEmitter<any>) {
    this._updateEntryGroupList$ = value;
  }


  get selectedEntryGroup(): EntryGroup {
    if (!this._selectedEntryGroup) {
      let localStorageEntryGroupString = localStorage.getItem('selectedEntryGroup')
      if (localStorageEntryGroupString) {
        this._selectedEntryGroup = this.entryGroupBuilder.build(JSON.parse(localStorageEntryGroupString));
      }
    }

    return this._selectedEntryGroup;
  }
  set selectedEntryGroup(value: EntryGroup) {
    this.selectedEntryGroupChangeEvent$.next(value);
    localStorage.setItem('selectedEntryGroup', value.toJsonString());
    this._selectedEntryGroup = value;
  }
  get selectedEntryGroupChangeEvent$(): BehaviorSubject<EntryGroup> {
    return this._selectedEntryGroupChangeEvent$;
  }

  constructor(
    private http: HttpClient,
    private dateFormatter: DatePipe,
    private entryBuilder: EntryBuilder,
    private entryGroupBuilder: EntryGroupBuilder
  ) {
    this.selectedEntryGroupChangeEvent$.next(this._selectedEntryGroup);
  }

  private _updateEntryGroupList$: EventEmitter<any> = new EventEmitter<any>();

  private _selectedEntryGroup: EntryGroup = null;
  private _selectedEntryGroupChangeEvent$: BehaviorSubject<EntryGroup> = new BehaviorSubject<EntryGroup>(null);

  getGroupedEntries(): Observable<EntryGroup[]> {
    return this.http.get<EntryGroup[]>(this.backendUrl + '/CallTradeApi/getWatchedEntriesGroups', {}).pipe(
      map(objectList => {
        let entryGroups = [];
        objectList.forEach(object => {
          entryGroups.push(this.entryGroupBuilder.build(object));
        });
        return entryGroups;
      })
    );
  }

  getEntriesByDate(from: Date, to: Date = null): Observable<Entry[]> {
    return this.http.post<Entry[]>(this.backendUrl + '/CallTradeApi/getWatchedEntriesByDateTime', {
      'from': this.dateFormatter.transform(from, 'yyyy-MM-dd'),
      'to': this.dateFormatter.transform(to, 'yyyy-MM-dd')
    }).pipe(
      map(objectList => {
        let entryList = [];
        objectList.forEach(object => {
          entryList.push(this.entryBuilder.build(object))
        })

        return entryList;
      })
    );
  }

  removeEntryFromWatchedList(entry: Entry): Observable<Entry> {
    return this.http.post<Entry>(this.backendUrl + '/CallTradeApi/watchEntry', {
      'entryId': entry.idEntry
    }).pipe(
      map(() => {
        return entry;
      })
    )
  }
}
