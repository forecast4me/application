import { Pair } from "./pair";
import { Base } from "./base";

export class Backtest extends Base{
  get time(): string {
    return this._time;
  }

  set time(value: string) {
    this._time = value;
  }

  get tolerance(): number {
    return this._tolerance;
  }

  set tolerance(value: number) {
    this._tolerance = value;
  }
  get idbacktest(): number {
    return this._idbacktest;
  }

  set idbacktest(value: number) {
    this._idbacktest = value;
  }

  get pair(): Pair {
    return this._pair;
  }

  set pair(value: Pair) {
    this._pair = value;
  }

  get interval(): number {
    return this._interval;
  }

  set interval(value: number) {
    this._interval = value;
  }

  get errorsCount(): number {
    return this._errorsCount;
  }

  set errorsCount(value: number) {
    this._errorsCount = value;
  }

  get rightsCount(): number {
    return this._rightsCount;
  }

  set rightsCount(value: number) {
    this._rightsCount = value;
  }

  get totalErrors(): number {
    return this._totalErrors;
  }

  set totalErrors(value: number) {
    this._totalErrors = value;
  }

  get totalRights(): number {
    return this._totalRights;
  }

  set totalRights(value: number) {
    this._totalRights = value;
  }

  get createdAt(): Date {
    return this._createdAt;
  }

  set createdAt(value: Date) {
    this._createdAt = value;
  }

  get updatedAt(): Date {
    return this._updatedAt;
  }

  set updatedAt(value: Date) {
    this._updatedAt = value;
  }
  public constructor(
    private _idbacktest: number,
    private _pair: Pair,
    private _interval: number,
    private _time: string,
    private _tolerance: number,
    private _errorsCount: number,
    private _rightsCount: number,
    private _totalErrors: number,
    private _totalRights: number,
    private _createdAt: Date,
    private _updatedAt: Date
  ) {
    super();
  }
}
