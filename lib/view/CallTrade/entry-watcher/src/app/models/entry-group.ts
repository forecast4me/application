import { Base } from "./base";

export class EntryGroup extends Base{
  public constructor(
    private _date: Date,
    private _rights: number,
    private _errors: number,
    private _waiting: number
  ) {
    super();
  }

  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }

  get rights(): number {
    return this._rights;
  }

  set rights(value: number) {
    this._rights = value;
  }

  get errors(): number {
    return this._errors;
  }

  set errors(value: number) {
    this._errors = value;
  }

  get waiting(): number {
    return this._waiting;
  }

  set waiting(value: number) {
    this._waiting = value;
  }

  public getTotalEntries(): number {
    return this.rights + this.errors + this.waiting;
  }

  public getTotalConfirmedEntries(): number {
    return this.rights + this.errors;
  }

  public getSuccessRate() {
    if (this.rights === 0) return 0;
    return this.getTotalConfirmedEntries() / this.rights;
  }

  public getErrorRate() {
    if (this.errors === 0) return 0;

    return this.getTotalConfirmedEntries() / this.errors;
  }
}
