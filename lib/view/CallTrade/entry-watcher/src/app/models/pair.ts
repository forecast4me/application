import { Base } from "./base";

export class Pair extends Base{
  get idpair(): number {
    return this._idpair;
  }

  set idpair(value: number) {
    this._idpair = value;
  }

  get currencyA(): string {
    return this._currencyA;
  }

  set currencyA(value: string) {
    this._currencyA = value;
  }

  get currencyB(): string {
    return this._currencyB;
  }

  set currencyB(value: string) {
    this._currencyB = value;
  }

  get interval(): number {
    return this._interval;
  }

  set interval(value: number) {
    this._interval = value;
  }
  public constructor(
    private _idpair: number,
    private _currencyA: string,
    private _currencyB: string,
    private _interval: number
  ) {
    super();
  }

  get pairLabel() {
    return this.currencyA + '/' + this.currencyB
  }
}
