import { Pair } from "./pair";
import { Backtest } from "./backtest";
import { Base } from "./base";

export class Entry extends Base {
  get idEntry(): number {
    return this._idEntry;
  }

  set idEntry(value: number) {
    this._idEntry = value;
  }

  get backtest(): Backtest {
    return this._backtest;
  }

  set backtest(value: Backtest) {
    this._backtest = value;
  }

  get time(): string {
    return this._time;
  }

  set time(value: string) {
    this._time = value;
  }

  get pair(): Pair {
    return this._pair;
  }

  set pair(value: Pair) {
    this._pair = value;
  }

  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }

  get operation(): number {
    return this._operation;
  }

  set operation(value: number) {
    this._operation = value;
  }

  get interval(): number {
    return this._interval;
  }

  set interval(value: number) {
    this._interval = value;
  }

  get tolerance(): number {
    return this._tolerance;
  }

  set tolerance(value: number) {
    this._tolerance = value;
  }

  get points(): number {
    return this._points;
  }

  set points(value: number) {
    this._points = value;
  }

  get status(): number {
    return this._status;
  }

  set status(value: number) {
    this._status = value;
  }

  public constructor(
    private _idEntry: number,
    private _pair: Pair,
    private _backtest: Backtest,
    private _date: Date,
    private _time: string,
    private _operation: number,
    private _interval: number,
    private _tolerance: number,
    private _points: number,
    private _status: number
  ) {
    super();
  }

  get statusText(): string {
    if (this.status == 2) {
      return 'Aguardando';
    } else if (this.status == 1) {
      return 'Ganho';
    } else {
      return 'Perda';
    }
  }

  get operationText(): string {
    if (this.operation == 1) {
      return 'CALL';
    }

    return 'PUT';
  }
}
