import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { EntryGroup } from "../../../models/entry-group";
import { EntryService } from "../../../services/entry.service";

@Component({
  selector: 'app-entry-index-list-item',
  templateUrl: './entry-index-list-item.component.html',
  styleUrls: ['./entry-index-list-item.component.scss']
})
export class EntryIndexListItemComponent {
  @Input() entryGroup: EntryGroup
  @HostListener("click") onClickEvent(){
    this.entryService.selectedEntryGroup = this.entryGroup;
  }

  constructor(
    public entryService: EntryService
  ) { }

  pluralMapping = {
    'entry': {
      '=0': 'Nenhuma entrada',
      '=1': '1 entrada',
      'other': '# entradas'
    },
    'confirmed': {
      '=0': '0 confirmadas',
      '=1': '1 confirmada',
      'other': '# confirmadas'
    },
    'rights': {
      '=0': 'Nenhum acerto',
      '=1': '1 acerto',
      'other': '# acertos'
    },
    'errors': {
      '=0': 'Nenhum erro',
      '=1': '1 erro',
      'other': '# erros'
    }
  }
}
