import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { EntryGroup } from "../../models/entry-group";

@Component({
  selector: 'entry-index-list',
  templateUrl: './entry-index-list.component.html',
  styleUrls: ['./entry-index-list.component.scss']
})
export class EntryIndexListComponent implements OnInit {
  @Input() entryGroupList: EntryGroup[]

  constructor() {
  }

  ngOnInit(): void {
  }
}
