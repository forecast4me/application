import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryIndexListItemComponent } from './entry-index-list-item.component';

describe('EntryIndexListItemComponent', () => {
  let component: EntryIndexListItemComponent;
  let fixture: ComponentFixture<EntryIndexListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryIndexListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryIndexListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
