import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryIndexListComponent } from './entry-index-list.component';

describe('EntryIndexListComponent', () => {
  let component: EntryIndexListComponent;
  let fixture: ComponentFixture<EntryIndexListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryIndexListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryIndexListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
