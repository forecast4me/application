import { Component, Input, ViewChild } from '@angular/core';
import { EntryService } from "../../services/entry.service";
import { Entry } from "../../models/entry";
import swal from "sweetalert";
import { MatSort } from "@angular/material/sort";

@Component({
  selector: 'entry-list',
  templateUrl: './entry-list.component.html',
  styleUrls: ['./entry-list.component.scss']
})

export class EntryListComponent {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @Input() entryList: Entry[];

  dataCols: string[] = ['status', 'time', 'pairLabel', 'timeframe', 'martingale', 'interval', 'tolerance', 'operation', 'actions'];

  constructor(
    public entryService: EntryService
  ) {
    this.entryService = entryService;
  }

  canShowEntryList(): boolean {
    return this.entryList.length > 0;
  }

  removeFromWatchedEntries(row: Entry) {
    swal({
      title: "Deseja parar de monitorar esta entrada?",
      text: "Você poderá monitorá-la novamente, mas terá que rodar o backtest dela novamente",
      icon: "warning",
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          swal("Removida da lista", {
            icon: "success",
          });

          this.removeEntryFromEntryList(row);

          return this.entryService.removeEntryFromWatchedList(row).subscribe(() => {
            this.entryService.updateEntryGroupList$.emit(true);
          });
        }
      });
  }

  public removeEntryFromEntryList(entry: Entry) {
    this.entryList = this.entryList.filter((element: Entry) => {
      return element.idEntry !== entry.idEntry
    })
  }
}
