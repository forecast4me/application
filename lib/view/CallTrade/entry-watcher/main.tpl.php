<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    include('lib/includes/head.php');
    ?>
    <script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="lib/view/CallTrade/entry-watcher/dist/entry-watcher/styles.6891710a5ea500981558.css">
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/runtime-es2015.1eba213af0b233498d9d.js" type="module"></script>
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/runtime-es5.1eba213af0b233498d9d.js" nomodule defer></script>
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/polyfills-es5.1fd9b76218eca8053895.js" nomodule defer></script>
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/polyfills-es2015.690002c25ea8557bb4b0.js" type="module"></script>
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/main-es2015.2721cc4b39dc42ac5880.js" type="module"></script>
    <script src="lib/view/CallTrade/entry-watcher/dist/entry-watcher/main-es5.2721cc4b39dc42ac5880.js" nomodule defer></script>
</head>
</head>
<body>
<div class="container">
    <?php include('lib/view/CallTrade/menu-top.part.php') ?>
</div>
<app-root></app-root>
</body>
</html>
