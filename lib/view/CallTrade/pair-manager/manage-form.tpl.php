<?php
/**
 * @var Pair[] $list
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php
    include('lib/includes/head.php');
    ?>
    <style href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"></style>
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('.data-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                }
            });
            var baseUrl = '<?=getenv('BASE_URL')?>CallTrade/';

            $('.data-table .delete').on("click", function () {
                let id = $(this).data("id");
                let name = $(this).data("name");
                let row = $(this).closest("tr");

                $.confirm({
                    title: 'Deseja apagar o registro \'' + name + '\'?',
                    content: '',
                    buttons: {
                        confirm: {
                            text: 'Sim',
                            btnClass: 'btn btn-danger',
                            keys: ['enter', 'space'],
                            action: function () {
                                this.backgroundDismiss = false;
                                this.buttons.confirm.setText('Apagando <i class="material-icons rotating">loop<i>');
                                this.buttons.confirm.addClass('flex-center pointer-default');
                                this.buttons.confirm.disable();
                                this.buttons.cancel.hide();

                                const $this = this;

                                $.post(baseUrl + 'removePair', {
                                    'pairId': id
                                }, function (data) {
                                    $this.close();
                                    if (data.status === false) {
                                        $.alert('Falha ao apagar o registro \'' + name + '\'');
                                        return;
                                    }

                                    $.alert({
                                        title: 'Registro \'' + name + '\' apagado',
                                        content: null,
                                        buttons: {
                                            confirm: {
                                                text: 'OK',
                                                btnClass: 'btn btn-default',
                                                action: function () {
                                                    row.children('td, th')
                                                        .animate({paddingBottom: 0, paddingTop: 0})
                                                        .wrapInner('<div />')
                                                        .children()
                                                        .slideUp();
                                                    table.row(row).remove().draw();
                                                }
                                            }
                                        }
                                    });
                                });

                                return false;
                            }
                        },
                        cancel: {
                            text: 'Não',
                            btnClass: 'btn btn-success',
                            keys: ['esc', 'backspace'],
                            action: function () {
                                return true;
                            }
                        }
                    }
                });
            })
        })
    </script>
</head>
<body>
<div class="container">
    <?php include('lib/view/CallTrade/menu-top.part.php') ?>

    <div class="row">
        <div class="col-12">
            <table class="table table-striped table-hover data-table">
                <thead>
                <tr>
                    <th>Par</th>
                    <th>Timeframe</th>
                    <th>Velas</th>
                    <th>Menor vela</th>
                    <th>Última vela</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($list)):
                    foreach ($list as $pair):
                        $lastCandleTimestamp = null;
                        $firstCandleTimestamp = null;
                        $lastCandleDatetime = PairService::getFirstOrLastCandleDatetime($pair, false);
                        if ($lastCandleDatetime instanceof DateTimeInterface) {
                            $lastCandleTimestamp = $lastCandleDatetime->getTimestamp();
                        }

                        $firstCandleDatetime = PairService::getFirstOrLastCandleDatetime($pair, true);
                        if ($firstCandleDatetime instanceof DateTimeInterface) {
                            $firstCandleTimestamp = $firstCandleDatetime->getTimestamp();
                        }
                        $totalCandles = PairService::getTotalCandlesByPair($pair);
                        ?>
                        <tr>
                            <td><?= $pair->getPairName(false) ?></td>
                            <td data-order="<?= $pair->interval ?>"><?= PairFormatter::getTimeframe($pair) ?></td>
                            <td data-order="<?= $totalCandles ?>"><?= NumberTools::parseInteger($totalCandles) ?></td>
                            <td data-order="<?= $lastCandleTimestamp ?? 0 ?>"><?= DateFormatter::parseDate($lastCandleDatetime, DateUtil::getTimezoneDiff()) ?></td>
                            <td data-order="<?= $firstCandleTimestamp ?? 0 ?>"><?= DateFormatter::parseDate($firstCandleDatetime, DateUtil::getTimezoneDiff()) ?></td>
                            <td>
                                <a href="#" target="_blank"
                                   class="delete"
                                   data-toggle="modal"
                                   data-id="<?= $pair->getIdpair() ?>"
                                   data-name="<?= $pair->getPairName() ?>"
                                ><i class="material-icons"
                                    data-toggle="tooltip"
                                    title="Delete">&#xE872;</i></a>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
