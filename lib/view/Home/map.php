<?php
	/**
	 * @var array $linhaCsv
	 * @var array $listaTipos
	 */
	$totalColunas = sizeof($linhaCsv);


	function getOptionsTipoColuna($listaTipos, $tipoSelecionado = NULL){
		$r = '';
		if(!$tipoSelecionado){
			$r .= "<option value=''>Não utilizado</option>";
		}
		foreach($listaTipos as $label=>$value){
			$r .= '<option value="'.$value.'" '.($value==$tipoSelecionado?'selected':'').'>'.$label.'</option>';
		}
		return $r;
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		include("lib/includes/head.php");
	?>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3>Mapeamento do CSV</h3>
			<h4>Selecione abaixo o conteúdo de cada coluna do CSV para que o sistema possa realizar o processamento:</h4>

			<form class="form-horizontal col-12" enctype="multipart/form-data" method="post">
				<p>Linha de mostra:</p>
				<table class="table">
					<thead>
						<tr>
						<?php
							for($i = 0; $i < $totalColunas; $i++):
						?>
								<td>
									<select name="col[]" class="form-control">
										<?=getOptionsTipoColuna($listaTipos)?>
									</select>
								</td>
						<?php
							endfor;
						?>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php
								foreach($linhaCsv as $cell){
									echo "<td class='text-center'>$cell</td>";
								}
							?>
						</tr>
					</tbody>
				</table>

				<div class="form-group">
					<button class="btn btn-default">Mapear</button>
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>
