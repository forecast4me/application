<?php
	/**
	 * @var BloodGroup[]|MysqlIterator $listaGruposSanguineos
	 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		include("lib/includes/head.php");
	?>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3>Ops, CSV incompatível ou nenhuma linha encontrada</h3>
			<a href="<?=URL::getURL('Home/index/')?>" class="btn btn-default">Tente novamente</a>
		</div>
	</div>
</div>

<script src="<?=URL::getURL('assets/js/bootstrap.min.js')?>"></script>

<script src="<?=URL::getURL('assets/plugins/jquery-toast-plugin-master/dist/jquery.toast.min.js')?>"></script>
<link href="<?=URL::getURL('assets/plugins/jquery-toast-plugin-master/dist/jquery.toast.min.css')?>" rel="stylesheet" />

<link href="<?=URL::getURL('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css')?>" rel="stylesheet">
<script src="<?=URL::getURL('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js')?>"></script>

<script>
	$(document).ready(function(){
		$(".datepicker").datepicker();
	});
</script>
</body>
</html>
