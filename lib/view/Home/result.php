<?php
	/**
	 * @var TradeTable $tradeTable
	 * @var $gmtReferencia
	 * @var $gmtBase
	 * @var $gmtCorretora
     * @var $filename
	 */

    /**
     * @param     $horaBase
     * @param int $offset
     * @return string
     */
	function compensarFuso($horaBase, $offset = 0){
		$time = new DateTime($horaBase);

		if($offset < 0){
			$h = $offset*-1;
			$interval = new DateInterval("PT".$h."H");
			$time = $time->sub($interval);
		}else if($offset > 0){
			$h = $offset;
			$interval = new DateInterval("PT".$h."H");
			$time = $time->add($interval);
		}

		return $time->format("H:i");
	}

    /**
     * @param TradeLine $tradeLine
     * @return string
     */
	function getCssClass($tradeLine){
		if($tradeLine->isPositive()){
			return "text-success";
		}else if($tradeLine->getAverageResult() < 0){
			return "text-danger";
		}

		return "";
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		include("lib/includes/head.php");
	?>
	<script>
		$(function(){
			$("#filtro").on("submit", function(e){
				e.preventDefault();
				var $table = $(".table-result");
				var $tbody = $("tbody", $table);
				var $currentMinimum = $("[name=percentualMinimo]").val();
				$currentMinimum = $currentMinimum.replace(/[^0-9]/gi, '')/100;
				$currentMinimum = parseFloat($currentMinimum);

				$("tr", $tbody).each(function(){
					var $value = $(this).data('value');
					$value = parseFloat($value);
					if($value < $currentMinimum){
						$(this).fadeOut(0);
					}else if($value >= $currentMinimum){
						$(this).fadeIn(0);
					}
				});
			});
		});
	</script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3>Resultados consolidados</h3>
            <?php
                if($filename):
            ?>
                <h4>
                    <strong>Arquivo: </strong><?=$filename?>
                </h4>
            <?php
                endif;
            ?>
			<form class="form-horizontal" id="filtro">
				<div class="row">
					<div class="col-3">
						<div class="col-12">
							<input type="text" name="percentualMinimo" class="form-control" placeholder="Percentual mínimo">
						</div>
					</div>
					<div class="col-3">
						<div class="col-12">
							<button type="submit" class="btn btn-success">Filtrar</button>
						</div>
					</div>
					<div class="col-3 pull-right">
						<div class="col-12 text-right">
							<a href="<?=URL::getURL('')?>" class="btn btn-default">Voltar</a>
						</div>
					</div>
				</div>
			</form>
			<table class="table table-stripped table-hover table-result">
				<thead>
					<tr>
						<th>Hora MT4</th>
                        <th>Resultado P/N</th>
                        <th>Score P/N</th>
						<th>Porcentagem</th>
						<th>Hora corretora</th>
						<th>Hora de Referência</th>
                        <th>Volume acumulado</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach($tradeTable->getTradeLines() as $tradeLine):
							$lineFrequency = $tradeLine->getHighestFrequency();
							$avgResult = $tradeLine->getAverageResult();
							$isPositive = $avgResult >= 0;
					?>
							<tr data-value='<?=$lineFrequency?>'>
								<td><?=$tradeLine->getLineTitle()?>h</td>
								<td>
                                    <strong class="<?=getCssClass($tradeLine)?>">
                                        <?=$tradeLine->getNominalResult()?>
                                    </strong> (<span class="text-success"><?=$tradeLine->getUpCount()?></span> / <?=$tradeLine->getNeutralCount()?> / <span class="text-danger"><?=$tradeLine->getDownCount()?></span>)
                                </td>
                                <td>
                                    <strong class="text-success"><?=$tradeLine->getUpTotal()?></strong> / <strong class="text-danger"><?=$tradeLine->getDownTotal()?></strong>
                                </td>
								<td class="<?=getCssClass($tradeLine)?>"><?=round(($lineFrequency*100), 4)?>%</td>
								<td><?=compensarFuso($tradeLine->getLineTitle(), $gmtCorretora - $gmtBase)?>h</td>
								<td><?=compensarFuso($tradeLine->getLineTitle(), $gmtReferencia - $gmtBase)?>h</td>
                                <td><?=$tradeLine->getVolumeTotal()?></td>
							</tr>
					<?php
						endforeach;
					?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">Relatório gerado às <?=date("d/m/Y H:i")?>h</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

</body>
</html>
