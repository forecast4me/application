<?php
	/**
	 * @var $dataInicio
	 * @var $dataFim
	 * @var $gmtReferencia
	 * @var $gmtBase
	 * @var $gmtCorretora
	 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		include("lib/includes/head.php");
	?>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3>Importação do CSV</h3>
			<h4>Faça o upload do arquivo .csv para realizar a consolidação dos dados.</h4>
			<form class="form-horizontal col-12" enctype="multipart/form-data" method="post">
				<div class="form-group">
					<div class="col-2">
						<div class="col-12">
							<input type="text" name="dataInicio" value='<?=DateUtil::dateConvert($dataInicio)?>' class="form-control datepicker" placeholder="Data de início">
						</div>
					</div>
					<div class="col-2">
						<div class="col-12">
							<input type="text" name="dataFim" value='<?=DateUtil::dateConvert($dataFim)?>' class="form-control datepicker" placeholder="Data de término">
						</div>
					</div>
					<div class="col-2">
						<div class="col-12">
							<input type="text" name="gmtReferencia" data-toggle="tooltip" value='<?=$gmtReferencia?$gmtReferencia:-3?>' class="form-control" placeholder="GMT Referência" data-toggle="tooltip" title="GMT Referência - Ex.: Brasil (-3)">
						</div>
					</div>
					<div class="col-2">
						<div class="col-12">
							<input type="text" name="gmtBase" data-toggle="tooltip" value="<?=$gmtBase?>" class="form-control" placeholder="GMT Sistema Base" title="GMT do sistema qual o CSV foi baixado">
						</div>
					</div>
					<div class="col-2">
						<div class="col-12">
							<input type="text" name="gmtCorretora" data-toggle="tooltip" value="<?=$gmtCorretora?>" class="form-control" placeholder="GMT Corretora" title="GMT da corretora">
						</div>
					</div>
				</div>

				<div class="form-group">
					Arquivo: <input type="file" name="file">
				</div>

				<div class="form-group">
					<label><input type="checkbox" name="mapData" value='1'> Utilizar assistente de mapeamento de colunas</label>
				</div>

				<div class="form-group">
					<button class="btn btn-default">Enviar arquivo</button>
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>
