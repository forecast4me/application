<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php
		include("lib/includes/head.php");
	?>
</head>
<body>
<div class="container">
	<div class="row">
		<!--Conteúdo da página-->
		<div class="col-xs-12 col-md-12 col-lg-12">
			<h3>Ops! Página não encontrada</h3>
		</div>
	</div>
</div>

<script src="<?=URL::getURL('assets/js/bootstrap.min.js')?>"></script>
</body>
</html>