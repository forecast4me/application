<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("pair")
 * @ORM\Entity
 */
class Pair
{
    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue()
     */
    public $idpair;

    /**
     * @return mixed
     */
    public function getIdpair()
    {
        return $this->idpair;
    }

    /**
     * @ORM\Column(type="string", name="currency_a")
     */
    public $currencyA;

    /**
     * @ORM\Column(type="string", name="currency_b")
     */
    public $currencyB;

    public function getPairName($includeTimeframe = true)
    {
        return $this->currencyA . '/' . $this->currencyB . ($includeTimeframe ? ' - ' . $this->interval . ' min' : '');
    }

    /**
     * @ORM\Column(type="integer", name="_interval")
     */
    public $interval;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     */
    public $updated_at;

    /**
     * @param DateTime $dateTime
     * @throws Exception
     */
    public function setUpdated($dateTime)
    {
        $this->updated_at = $dateTime;
    }

    /**
     * @ORM\Column(type="integer", name="last_line_read")
     */
    public $lastLineRead;

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->currencyA . $this->currencyB . $this->interval . '.csv';
    }
}
