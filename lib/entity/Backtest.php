<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("backtest")
 * @ORM\Entity(repositoryClass="BacktestRepository")
 */
class Backtest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $idbacktest;

    /**
     * @ORM\ManyToOne(targetEntity="Pair")
     * @ORM\JoinColumn(name="pair_idpair", referencedColumnName="idpair")
     */
    public $pair;

    /**
     * @return Pair
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * @param mixed $pair
     */
    public function setPair($pair)
    {
        $this->pair = $pair;
    }

    /**
     * @return int
     */
    public function getIdBacktest()
    {
        return $this->idbacktest;
    }

    /**
     * @ORM\Column(type="integer", name="_interval")
     */
    public $interval;

    /**
     * @ORM\Column(type="string", name="_time")
     */
    public $time;

    /**
     * @ORM\Column(type="float")
     */
    public $tolerance;

    /**
     * @ORM\Column(type="integer", name="errors_count")
     */
    public $errorsCount = 0;

    /**
     * @ORM\Column(type="integer", name="rights_count")
     */
    public $rightsCount = 0;

    /**
     * @ORM\Column(type="integer", name="sequence_errors")
     */
    public $sequenceErrors = 0;

    /**
     * @ORM\Column(type="integer", name="sequence_rights")
     */
    public $sequenceRights = 0;

    /**
     * @ORM\Column(type="integer", name="total_errors")
     */
    public $totalErrors = 0;

    /**
     * @ORM\Column(type="integer", name="total_rights")
     */
    public $totalRights = 0;

    /**
     * @ORM\Column(type="string", name="created_at")
     */
    public $createdAt;

    /**
     * @ORM\Column(type="string", name="updated_at")
     */
    public $updatedAt;

    public function setUpdated($date)
    {
        $this->updatedAt = $date;
    }

    public function sumErrors()
    {
        if ($this->errorsCount >= $this->sequenceErrors) {
            $this->sequenceErrors++;
        }

        $this->rightsCount = 0;
        $this->errorsCount++;
        $this->totalErrors++;
    }

    public function sumRights()
    {
        if ($this->rightsCount >= $this->sequenceRights) {
            $this->sequenceRights++;
        }

        $this->errorsCount = 0;
        $this->rightsCount++;
        $this->totalRights++;
    }

    public function calcSuccessRatio(){
        $total = $this->totalRights + $this->totalErrors;
        if($total === 0) return 0;
        return round(($this->totalRights / $total), 2);
    }
}
