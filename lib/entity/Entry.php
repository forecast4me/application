<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("entry")
 * @ORM\Entity(repositoryClass="EntryRepository")
 */
class Entry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    public $identry;

    /**
     * @ORM\OneToOne(targetEntity="Pair", fetch="EAGER")
     * @ORM\JoinColumn(name="pair_idpair", referencedColumnName="idpair")
     * @var Pair
     */
    public $pair;

    /**
     * @ORM\OneToOne(targetEntity="Backtest", fetch="EAGER")
     * @ORM\JoinColumn(name="backtest_idbacktest", referencedColumnName="idbacktest")
     * @var Backtest
     */
    public $backtest;

    /**
     * @ORM\Column(type="integer")
     */
    public $operation;

    /**
     * @ORM\Column(type="string", name="_date")
     */
    public $date;

    /**
     * @ORM\Column(type="string", name="_time")
     */
    public $time;

    /**
     * @ORM\Column(type="integer", name="_interval")
     */
    public $interval;

    /**
     * @ORM\Column(type="float", name="tolerance")
     */
    public $tolerance;

    /**
     * @ORM\Column(type="float")
     */
    public $points;

    /**
     * @ORM\Column(type="integer", name="_status")
     */
    public $status;

    /**
     * @ORM\Column(type="string", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="watched")
     */
    protected $watched;

    /**
     * @return Candle
     */
    public function toCandle(): ?\Candle
    {
        $c = new Candle();
        $c->pair = $this->pair;
        $c->date = $this->date;
        $c->time = $this->time;
        $c->interval = $this->interval;
        $c->points = $this->points;
        $c->result = $this->operation === OperationEnum::CALL ? -1 : 1;
        return $c;
    }

    /**
     * @return int
     */
    public function getIdEntry(): ?int
    {
        return $this->identry;
    }

    /**
     * @return Pair
     */
    public function getPair(): Pair
    {
        return $this->pair;
    }

    /**
     * @param Pair $pair
     */
    public function setPair(Pair $pair): void
    {
        $this->pair = $pair;
    }

    /**
     * @return Backtest
     */
    public function getBacktest(): Backtest
    {
        return $this->backtest;
    }

    /**
     * @param Backtest $backtest
     */
    public function setBacktest(Backtest $backtest): void
    {
        $this->backtest = $backtest;
    }

    /**
     * @return mixed
     */
    public function getTolerance()
    {
        return $this->tolerance;
    }

    /**
     * @param mixed $tolerance
     */
    public function setTolerance($tolerance): void
    {
        $this->tolerance = $tolerance;
    }

    /**
     * @return mixed
     */
    public function getWatched()
    {
        return $this->watched ?? false;
    }

    /**
     * @param mixed $watched
     */
    public function setWatched($watched): void
    {
        $this->watched = $watched;
    }
}
