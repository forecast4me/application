<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("candle")
 * @ORM\Entity(repositoryClass="CandleRepository")
 */
class Candle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $idcandle;

    /**
     * @ORM\OneToOne(targetEntity="Pair")
     * @ORM\JoinColumn(name="pair_idpair", referencedColumnName="idpair")
     */
    public $pair;

    /**
     * @ORM\Column(type="string", name="_date")
     */
    public $date;

    /**
     * @ORM\Column(type="string", name="_time")
     */
    public $time;

    /**
     * @ORM\Column(type="float", name="_interval")
     */
    public $interval;

    /**
     * @ORM\Column(type="float", name="_open")
     */
    public $open;

    /**
     * @ORM\Column(type="float", name="_max")
     */
    public $max;

    /**
     * @ORM\Column(type="float", name="_min")
     */
    public $min;

    /**
     * @ORM\Column(type="float", name="_close")
     */
    public $close;

    /**
     * @ORM\Column(type="float")
     */
    public $volume;

    /**
     * @ORM\Column(type="float")
     */
    public $points;

    /**
     * @ORM\Column(type="smallint")
     */
    public $result;

    public function calculatePoints()
    {
        return $this->points;
    }

    public function getHash()
    {
        return $this->time;
    }

    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getIdcandle()
    {
        return $this->idcandle;
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    public function getDate()
    {
        return new DateTime($this->date);
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDateTimeString()
    {
        return $this->date . ' ' . $this->time;
    }
}
