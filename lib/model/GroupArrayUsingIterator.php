<?php

use Doctrine\ORM\Tools\Pagination\Paginator;

class GroupArrayUsingIterator implements Iterator, Countable
{
    protected $array;
    protected $index;
    protected $maxElementsInGroup;
    protected $totalElements;
    protected $totalGroups;
    /**
     * @var FILOArray
     */
    protected $return;
    protected $step;

    /**
     * CandleIterator constructor.
     * @param array | Iterator | Paginator $collection
     * @param $maxElementsInGroup - Quantity of elements to return
     * @param $lifoClassName string
     * @throws Exception
     */
    public function __construct($collection, $maxElementsInGroup, $lifoClassName)
    {
        $this->array = $collection->getIterator();
        $this->maxElementsInGroup = $maxElementsInGroup;
        $this->totalElements = count($collection);
        $this->totalGroups = (int)round($this->totalElements - $this->maxElementsInGroup) + 1;

        for ($this->index = 0; $this->index < $this->maxElementsInGroup; $this->index++) {
            if ($this->array->valid()) {
                if($this->index > 0){
                    $this->array->next();
                }

                $arr[] = $this->array->current();
            }
        }
        $this->index--;

        $this->return = new $lifoClassName($arr);
    }

    /**
     * Return the current element
     * @link  https://php.net/manual/en/iterator.current.php
     * @return FILOArray
     * @since 5.0.0
     */
    public function current(): FILOArray
    {
        return $this->return;
    }

    /**
     * Move forward to next element
     * @link  https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->index++;
        $this->array->next();
        if($this->array->valid()) {
            $this->return->add($this->array->current());
            $this->step++;
        }
    }

    /**
     * Return the key of the current element
     * @link  https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->step;
    }

    /**
     * Checks if current position is valid
     * @link  https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return $this->index < $this->totalElements;
    }

    /**
     * Rewind the Iterator to the first element
     * @link  https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->step = 0;
        $this->index = 0;
        $this->array->rewind();
    }

    /**
     * Count elements of an object
     * @link  https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count(): int
    {
        return $this->totalGroups;
    }
}
