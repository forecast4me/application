<?php


class EntryWatchedGroup implements JsonSerializable
{
    private $date;

    private $rights;

    private $errors;

    private $waiting;

    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    public function getDate()
    {
        return new DateTime($this->date);
    }

    /**
     * @param mixed $date
     * @return EntryWatchedGroup
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRights()
    {
        return $this->rights ?? 0;
    }

    /**
     * @param mixed $rights
     * @return EntryWatchedGroup
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors ?? 0;
    }

    /**
     * @param mixed $errors
     * @return EntryWatchedGroup
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWaiting()
    {
        return $this->waiting ?? 0;
    }

    /**
     * @param mixed $waiting
     * @return EntryWatchedGroup
     */
    public function setWaiting($waiting)
    {
        $this->waiting = $waiting;
        return $this;
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function jsonSerialize()
    {
        return [
            'date' => $this->getDate()->format('Y-m-d'),
            'waiting' => (int) $this->getWaiting(),
            'rights' => $this->getRights(),
            'errors' => $this->getErrors()
        ];
    }
}
