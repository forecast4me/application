<?php


class EntryStatusEnum
{
    const FAIL = 0;
    const SUCCESS = 1;
    const WAITING = 2;
}
