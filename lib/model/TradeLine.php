<?php
/**
 * Código desenvolvido por Renan
 * Criado em: 13/05/17 às 00:59
 * Todos os códigos deste projeto são de propriedade do autor, mas com licença de uso ao cliente
 */

class TradeLine
{
    private $_lineTitle, $_tradeLineRegisters, $_upCount, $_upTotal, $_downCount, $_downTotal, $_volumeTotal;

    public function __construct($lineTitle)
    {
        $this->setLineTitle($lineTitle);
    }


    /**
     * @param mixed $downFrequency
     */
    public function setDownCount($downFrequency)
    {
        $this->_downCount = $downFrequency;
    }

    /**
     * @return mixed
     */
    public function getDownCount()
    {
        if (!$this->_downCount) return 0;
        return $this->_downCount;
    }

    public function increaseDownCount()
    {
        $this->_downCount++;
    }

    public function getDownFrequency()
    {
        return $this->getDownCount() / $this->getTotalTradeLineRegisters();
    }


    /**
     * @param mixed $upFrequency
     */
    public function setUpCount($upFrequency)
    {
        $this->_upCount = $upFrequency;
    }

    /**
     * @return mixed
     */
    public function getUpCount()
    {
        if (!$this->_upCount) return 0;
        return $this->_upCount;
    }

    public function increaseUpCount()
    {
        $this->_upCount++;
    }

    public function getUpFrequency()
    {
        return $this->getUpCount() / $this->getTotalTradeLineRegisters();
    }


    /**
     * @param mixed $tradeLineRegisters
     */
    public function setTradeLineRegisters($tradeLineRegisters)
    {
        $this->_tradeLineRegisters = $tradeLineRegisters;
    }

    /**
     * @return mixed
     */
    public function getTradeLineRegisters()
    {
        return $this->_tradeLineRegisters;
    }

    public function getTotalTradeLineRegisters(): int
    {
        return count($this->_tradeLineRegisters);
    }

    public function addRegistro($data)
    {
        if ($tradeLineRegister instanceof TradeLineRegister) {
            $r = $data;
        } else {
            $r = new TradeLineRegister($data);
        }

        if ($r->getPriceResult() > 0) {
            $this->increaseUpCount();
            $this->sumUpTotal($r->getPriceScore());
        } else if ($r->getPriceResult() < 0) {
            $this->increaseDownCount();
            $this->sumDownTotal($r->getPriceScore());
        }

        $this->sumVolumeTotal($r->volume);
        $this->_tradeLineRegisters[] = $r;
    }


    public function sumVolumeTotal($volume)
    {
        $this->_volumeTotal += $volume;
    }

    public function getVolumeTotal()
    {
        return $this->_volumeTotal;
    }


    public function sumUpTotal($number)
    {
        $this->_upTotal += $number;
    }

    public function getUpTotal()
    {
        return $this->_upTotal;
    }

    public function sumDownTotal($number)
    {
        $this->_downTotal += $number;
    }

    public function getDownTotal()
    {
        return $this->_downTotal;
    }


    /**
     * @return mixed
     */
    public function getLineTitle()
    {
        return $this->_lineTitle;
    }

    /**
     * @param mixed $lineTitle
     */
    public function setLineTitle($lineTitle)
    {
        $this->_lineTitle = $lineTitle;
    }

    public function getAverageResult()
    {
        return $this->getUpCount() - $this->getDownCount();
    }

    public function getHighestFrequency()
    {
        if ($this->getUpFrequency() > $this->getDownFrequency()) {
            return $this->getUpFrequency();
        }

        if ($this->getUpFrequency() < $this->getDownFrequency()) {
            return $this->getDownFrequency();
        }

        return $this->getDownFrequency();
    }

    public function isPositive()
    {
        return $this->getAverageResult() > 0;
    }

    public function getNominalResult()
    {
        if ($this->isPositive()) {
            return 'Positivo';
        }

        if ($this->getAverageResult() < 0) {
            return 'Negativo';
        }

        return 'Neutro';
    }

    public function getNeutralCount()
    {
        return $this->getTotalTradeLineRegisters() - ($this->getUpCount() + $this->getDownCount());
    }
}
