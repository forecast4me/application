<?php

use Doctrine\ORM\Tools\Pagination\Paginator as PaginatorAlias;

class GroupIterator implements Iterator, Countable
{
    protected $iterator;
    protected $numElements;
    /**
     * @var FILOArray
     */
    protected $return;
    protected $step;

    /**
     * CandleIterator constructor.
     * @param PaginatorAlias $collection
     * @param $numElements - Quantity of elements to return
     * @param $fixedSizeArrayClass string
     * @throws Exception
     */
    public function __construct($collection, $numElements, $fixedSizeArrayClass)
    {
        $this->iterator = $collection->getIterator();
        $this->numElements = $numElements;

        for ($i = 0; $i < $this->numElements; $i++) {
            $this->iterator->next();
            if ($this->iterator->valid()) {
                $arr[] = $this->iterator->current();
            }
        }

        $this->return = new $fixedSizeArrayClass($arr);
    }

    /**
     * Return the current element
     * @link  https://php.net/manual/en/iterator.current.php
     * @return FILOArray
     * @since 5.0.0
     */
    public function current(): FILOArray
    {
        return $this->return;
    }

    /**
     * Move forward to next element
     * @link  https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->iterator->next();
        if ($this->iterator->valid()) {
            $this->return->add($this->iterator->current());
            $this->step++;
        }
    }

    /**
     * Return the key of the current element
     * @link  https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->step;
    }

    /**
     * Checks if current position is valid
     * @link  https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    /**
     * Rewind the Iterator to the first element
     * @link  https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->step = 0;
        $this->iterator->rewind();
    }

    /**
     * Count elements of an object
     * @link  https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count(): int
    {
        return $this->iterator->count();
    }
}
