<?php
/**
 * Código desenvolvido por Renan
 * Criado em: 13/05/17 às 01:01
 * Todos os códigos deste projeto são de propriedade do autor, mas com licença de uso ao cliente
 */

class TradeTable
{
    private $_tradeLines, $_interval;

    /**
     * @param mixed $interval
     */
    public function setInterval($interval)
    {
        $this->_interval = $interval;
    }

    /**
     * @return mixed
     */
    public function getInterval()
    {
        return $this->_interval;
    }

    /**
     * @param mixed $tradeLines
     */
    public function setTradeLines($tradeLines)
    {
        $this->_tradeLines = $tradeLines;
    }

    /**
     * @return TradeLine[]
     */
    public function getTradeLines(): array
    {
        return $this->_tradeLines;
    }

    /**
     * TradeTable constructor.
     * @param $arr
     * @param null $dataInicio
     * @param null $dataFim
     * @throws Exception
     */
    public function __construct($arr, $dataInicio = NULL, $dataFim = NULL)
    {
        if ($dataInicio) {
            $dataInicio = new DateTime($dataInicio);
        }

        if ($dataFim) {
            $dataFim = new DateTime($dataFim);
        }

        foreach ($arr as $index => $row) {
            $dataRegistro = $row[0];
            $dataRegistro = str_replace(['.', '/'], '-', $dataRegistro);

            $horaRegistro = $row[1];
            try {
                $dt = new DateTime($dataRegistro . ' ' . $horaRegistro);
            } catch (Exception $e) {
                SystemNotificator::getInstance()->addNotification('Inconsistência no registro ' .$index. '. Falha ao converter data');
                break;
            }

            //VALIDA O RANGE
            if ($dataInicio && $dataFim) {
                if ($dt >= $dataInicio && $dt <= $dataFim) {
                    $line = $this->getLineByHash($row[1]);
                    $line->addRegistro($row);
                }
            } else if ($dataInicio && !$dataFim) {
                if ($dt >= $dataInicio) {
                    $line = $this->getLineByHash($row[1]);
                    $line->addRegistro($row);
                }
            } else if (!$dataInicio && $dataFim) {
                if ($dt <= $dataFim) {
                    $line = $this->getLineByHash($row[1]);
                    $line->addRegistro($row);
                }
            } else {
                $line = $this->getLineByHash($row[1]);
                $line->addRegistro($row);
            }
        }
    }

    /**
     * @param $hash
     * @return TradeLine
     */
    public function getLineByHash($hash): TradeLine
    {
        if (!$this->_tradeLines[$hash]) {
            $this->_tradeLines[$hash] = new TradeLine($hash);
        }
        return $this->_tradeLines[$hash];
    }

    public function sort($fn)
    {
        $tmp = $this->getTradeLines();
        usort($tmp, $fn);
        $this->setTradeLines($tmp);
    }
}
