<?php

/**
 * Class FILOArray
 * First-in-last-out
 */
class FILOArray implements Countable
{
    protected $arr;
    protected $size;

    /**
     * FILOArray constructor.
     * @param $arr
     * @throws Exception
     */
    public function __construct($arr)
    {
        if (!is_array($arr)) {
            throw new \RuntimeException('FILOArray requires an array as input. Other type given');
        }
        $this->size = count($arr);
        if (!$this->size) {
            throw new \RuntimeException('Input array must have elements for new FILOArray instances');
        }

        $this->arr = $arr;
    }

    public function add($element)
    {
        $this->arr[] = $element;
        array_shift($this->arr);
    }

    public function getLastElement()
    {
        return $this->arr[($this->size - 1)];
    }

    /**
     * @return mixed
     */
    public function getArray()
    {
        return $this->arr;
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->arr);
    }
}
