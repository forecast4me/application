<?php

class CallTrade
{
    public function __construct($arr, $dataInicio = NULL, $dataFim = NULL)
    {
        try {
            if ($dataInicio) {
                $dataInicio = new DateTime($dataInicio);
            }

            if ($dataFim) {
                $dataFim = new DateTime($dataFim);
            }

            foreach ($arr as $index => $row) {
                $dataRegistro = $row[0];
                $dataRegistro = str_replace(['.', '/'], "-", $dataRegistro);

                $horaRegistro = $row[1];
                try {
                    $dt = new DateTime($dataRegistro . " " . $horaRegistro);
                } catch (Exception $e) {
                    SystemNotificator::addNotification("Inconsistência no registro $index. Falha ao converter data");
                    break;
                }

                //VALIDA O RANGE
                if ($dataInicio && $dataFim) {
                    if ($dt >= $dataInicio && $dt <= $dataFim) {
                        $line = $this->getLineByHash($row[1], $row);
                        $line->addRegistro($row);
                    }
                } else if ($dataInicio && !$dataFim) {
                    if ($dt >= $dataInicio) {
                        $line = $this->getLineByHash($row[1], $row);
                        $line->addRegistro($row);
                    }
                } else if (!$dataInicio && $dataFim) {
                    if ($dt <= $dataFim) {
                        $line = $this->getLineByHash($row[1], $row);
                        $line->addRegistro($row);
                    }
                } else {
                    $line = $this->getLineByHash($row[1], $row);
                    $line->addRegistro($row);
                }
            }
        } catch (Exception $e) {

        }
    }
}
