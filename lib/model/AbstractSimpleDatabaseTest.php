<?php


use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

abstract class AbstractSimpleDatabaseTest extends TestCase
{
    /**
     * @var EntityManager
     */
    private $em;
    private $className;
    /**
     * @var array
     */
    private $classArgs;
    /**
     * @var object
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();
        $this->em = DB::getInstance()->getEntityManager();

        $this->setClassName('');
        $this->setClassArgs();

        $this->instance = $this->_getClassInstance();
    }

    /**
     * @param $class
     */
    public function setClassName($class): void
    {
        $this->className = $class;
    }

    /**
     * @param mixed ...$args
     */
    public function setClassArgs(...$args): void
    {
        $this->classArgs = $args;
    }

    /**
     * @return object
     * @throws ReflectionException
     */
    protected function _getClassInstance()
    {
        if (!isset($this->className)) {
            throw new RuntimeException('Class name not set. Define the class to be tested using setClassName on your setUp');
        }

        $reflection = new ReflectionClass($this->className);
        if(isset($this->classArgs[0])) {
            return $reflection->newInstance(...$this->classArgs ?? null);
        }

        return $reflection->newInstance();
    }

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em): void
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getInstance()
    {
        return $this->instance;
    }

}
