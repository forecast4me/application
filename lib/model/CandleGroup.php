<?php


class CandleGroup extends FILOArray
{
    protected $upCount, $downCount, $upRate, $downRate;

    /**
     * @return mixed
     */
    public function getDownrate()
    {
        return $this->downRate;
    }

    /**
     * @return mixed
     */
    public function getUprate()
    {
        return $this->upRate;
    }

    /**
     * CandleGroup constructor.
     * @param Candle[] $arr
     * @throws Exception
     */
    public function __construct($arr)
    {
        parent::__construct($arr);

        //Update intern counters
        $this->_updateCounters();
    }

    /**
     * @param Candle $element
     */
    public function add($element)
    {
        parent::add($element);

        //Update intern counters after add candle
        $this->_updateCounters();
    }

    /**
     */
    protected function _updateCounters(): void
    {
        $this->upCount = 0;
        $this->downCount = 0;

        if (count($this->getArray())) {
            foreach ($this->getArray() as $candle) {
                $candle->getResult() > 0 ? $this->upCount++ : $this->downCount++;
            }
        }

        $this->_calcRates();
    }

    protected function _calcRates(): void
    {
        $this->upRate = $this->upCount / $this->size;
        $this->downRate = $this->downCount / $this->size;
    }

    /**
     * @return Candle[] array
     */
    public function getArray()
    {
        return parent::getArray();
    }

    public function getMaxRate()
    {
        return max($this->upRate, $this->downRate);
    }

    public function getDominantResult()
    {
        return $this->upCount > $this->downCount ? 1 : -1;
    }
}
