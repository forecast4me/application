<?php
/**
 * Código desenvolvido por Renan
 * Criado em: 14/05/17 às 19:46
 * Todos os códigos deste projeto são de propriedade do autor, mas com licença de uso ao cliente
 */

class TradeLineRegister
{
    public $date, $time, $openValue, $highestValue, $lowestValue, $closeValue, $volume;

    public function __construct($data = null)
    {
        $this->date = $data[0];
        $this->time = $data[1];
        $this->openValue = $data[2];
        $this->highestValue = $data[3];
        $this->lowestValue = $data[4];
        $this->closeValue = $data[5];
        $this->volume = $data[6];
    }

    public function getPriceScore()
    {
        $openValue = (double)$this->openValue;
        $closeValue = (double)$this->closeValue;

        $r = $closeValue - $openValue;
        $r = round($r, 5);

        return $r;
    }

    public function getPriceResult()
    {
        if ($this->getPriceScore() > 0) {
            return 1;
        } else if ($this->getPriceScore() < 0) {
            return -1;
        }

        return 0;
    }
}
