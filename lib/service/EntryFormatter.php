<?php

class EntryFormatter
{
    /**
     * @param Entry $entry
     * @return string
     */
    public static function getDate(Entry $entry): string
    {
        try {
            $date = new DateTime($entry->date);
        } catch (Exception $e) {
            return 'N/A';
        }
        return $date->format('d/m/Y');
    }

    /**
     * @param Entry $entry
     * @return string
     * @throws Exception
     */
    public static function getTime(Entry $entry): string
    {
        return DateFormatter::parseTime($entry->time);
    }

    /**
     * @param Entry $entry
     * @return string
     */
    public static function getOperation(Entry $entry): string
    {
        return $entry->operation === OperationEnum::PUT ? 'PUT' : 'CALL';
    }

    public static function getPoints(Entry $entry) : string
    {
        if(!$entry->points) return 'N/A';
        return sprintf('%.6f', (float) $entry->points);
    }
}
