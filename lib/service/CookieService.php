<?php


class CookieService extends AbstractSingleton
{
    private const COOKIE_NAME = 'calltrade';

    /**
     * @param $key
     * @param $value
     * @param int $days
     */
    public function set($key, $value, $days = 30): void
    {
        $data = $this->getCookie();
        $data[$key] = $value;
        $this->setCookie($data, $days);
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key)
    {
        $data = $this->getCookie();

        return $data[$key] ?? null;
    }

    /**
     * @return mixed|null
     */
    protected function getCookie()
    {
        $data = $_COOKIE[self::COOKIE_NAME];
        if (null === $data) {
            return null;
        }

        return json_decode($data, true);
    }

    /**
     * @param $data
     * @param int $days
     */
    protected function setCookie($data, $days = 30): void
    {
        $data = json_encode($data);
        setcookie(self::COOKIE_NAME, $data, time() + (86400 * $days), '/');
    }
}
