<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

class BacktestService
{
    /**
     * @var EntityManager
     */
    public $em;
    private $entryService;
    private $totalGeneratedEntries;

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Backtest constructor.
     * @param EntityManager $em
     * @param EntryService $entryService
     */
    public function __construct($em, $entryService)
    {
        $this->em = $em;
        $this->entryService = $entryService;
    }

    /**
     * @param $idpair
     * @param $interval
     * @param $tolerance
     * @return Backtest[] | boolean
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function start($idpair, $interval, $tolerance)
    {
        if ($tolerance > 1) {
            throw new InvalidArgumentException('A tolerância precisa ser menor que 100%');
        }

        if ($tolerance <= 0.5) {
            throw new InvalidArgumentException('A tolerância precisa ser maior que 50');
        }

        $pairRepository = $this->getEm()->getRepository('Pair');

        /**
         * @var Pair $pair
         */
        $pair = $pairRepository->find($idpair);

        if (!$idpair) {
            return false;
        }

        //Get all the available time keys
        /**
         * @var CandleRepository $candleRepository
         */
        $candleRepository = $this->getEm()->getRepository(Candle::class);

        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->getEm()->getRepository(Entry::class);

        $timeKeys = $candleRepository->getTimeKeysByIdPair($idpair);

        //If has no time keys available
        if (!count($timeKeys)) {
            return false;
        }

        /**
         * @var BacktestRepository $backtestRepository
         */
        $backtestRepository = $this->getEm()->getRepository('Backtest');
        foreach ($timeKeys as $timeKey) {
            //Do backtests from the last updated date
            $backtest = $backtestRepository->findLastBacktestByPairTimeIntervalTolerance($idpair, $timeKey, $interval, $tolerance);

            if (!$backtest) {
                $backtest = new Backtest();
                $backtest->time = $timeKey;
                $backtest->tolerance = $tolerance;
                $backtest->interval = $interval;
                $backtest->updatedAt = DateUtil::OLDEST_DATE_STRING;
                $backtest->setPair($pair);
                $this->getEm()->persist($backtest);
            }

            /**
             * @var CandleRepository $candleRepository
             */
            $candleRepository = $this->getEm()->getRepository('Candle');

            $lastEntry = $entryRepository->findByLatestEntryByPairTimekeyIntervalTolerance($pair, $timeKey, $interval, $tolerance);
            $lastCandle = $candleRepository->findByLatestCandleByPairTimekeyInterval($pair, $timeKey, $pair->interval);
            if(
                null !== $lastEntry
                && null !== $lastCandle
                && strtotime($lastEntry->date) > strtotime($lastCandle->date)
            ) {
                continue;
            }

            $candleList = $candleRepository->findCandlesForBacktest($pair, $backtest->interval, $backtest->updatedAt, $timeKey);
            $totalCandles = count($candleList);
            if ($totalCandles && $totalCandles >= $backtest->interval) {
                $candleGroups = new GroupArray($candleList, $backtest->interval, CandleGroup::class);
                if (!$candleGroups) {
                    continue;
                }

                $sample = $candleGroups->current();

                /**
                 * @var Candle $candle
                 */
                $candle = $sample->getLastElement();

                $entry = $entryRepository->findByPairDateTimeInterval($pair, $candle->date, $timeKey, $interval, $tolerance);

                $totalCandleGroupsToCheck = count($candleGroups);
                for ($i = 0; $i < $totalCandleGroupsToCheck; $i++) {
                    if ($candleGroups->valid()) {
                        $sample = $candleGroups->current();
                    }

                    $candle = $sample->getLastElement();

                    if (($entry ?? false) && $entry->status === EntryStatusEnum::WAITING) {
                        /**
                         * @var Candle $candle
                         * @var Entry $entry
                         */
                        if ($this->entryService->checkEntryResult($entry, $candle)) {
                            $backtest->sumRights();
                            $entry->points = abs($candle->points);
                            $entry->status = EntryStatusEnum::SUCCESS;
                        } else {
                            $backtest->sumErrors();
                            $entry->points = abs($candle->points) * -1;
                            $entry->status = EntryStatusEnum::FAIL;
                        }
                    }

                    /**
                     * @var CandleGroup $candleGroup
                     */
                    $entry = $this->entryService->canGenerateEntry($sample, $tolerance) ? $this->entryService->generateEntryByCandleGroupAndBacktest($sample, $backtest) : null;

                    if (null !== $entry) {
                        $this->sumGeneratedEntries();
                    }

                    $candleGroups->next();
                }
            }

            //Saves the last generated entry if exists
            if (isset($entry)) {
                //Checks if entry exists before saving
                $entryDb = $entryRepository->findByPairDateTimeInterval(
                    $entry->getPair(),
                    $entry->date,
                    $entry->time,
                    $entry->interval,
                    $entry->tolerance
                );

                if (null === $entryDb) {
                    $this->entryService->save($entry);
                }
            }

            if (isset($candle)) {
                $backtest->setUpdated($candle->getDateTimeString());
            }

            $this->getEm()->persist($backtest);
        }

        $this->getEm()->flush();
        return true;
    }

    /**
     * @return mixed
     * @codeCoverageIgnore
     */
    public function getTotalGeneratedEntries()
    {
        return $this->totalGeneratedEntries;
    }

    /**
     * @param $value
     * @codeCoverageIgnore
     */
    public function setTotalGeneratedEntries($value): void
    {
        $this->totalGeneratedEntries = $value;
    }

    private function sumGeneratedEntries(): void
    {
        $this->totalGeneratedEntries++;
    }
}
