<?php


use Doctrine\DBAL\DBALException;

class PairFormatter
{
    /**
     * @param Pair $pair
     * @return string
     */
    public static function getTimeframe(Pair $pair): string
    {
        return $pair->interval.' min';
    }
}
