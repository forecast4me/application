<?php


class DateFormatter
{

    /**
     * @param DateTime | string | null $datetime
     * @param null $timezoneDiff
     * @return string
     * @throws Exception
     */
    public static function parseDate($datetime, $timezoneDiff = null): string
    {
        if (null === $datetime || !($datetime instanceof DateTime)) {
            return 'n/d';
        }

        if (null !== $timezoneDiff && is_numeric($timezoneDiff)) {
            $datetime = DateUtil::normalize($datetime, DateUtil::getTimezoneDiff());
        }

        return $datetime->format('d/m/Y');
    }

    /**
     * @param $time
     * @param null $timezoneDiff
     * @return mixed
     * @throws Exception
     */
    public static function parseTime($time, $timezoneDiff = null)
    {
        $datetime = new DateTime($time);
        if (null === $datetime || !($datetime instanceof DateTime)) {
            return 'n/d';
        }

        if (null !== $timezoneDiff && is_numeric($timezoneDiff)) {
            $datetime = DateUtil::normalize($datetime, $timezoneDiff);
        }

        return $datetime->format('H:i:s');
    }
}
