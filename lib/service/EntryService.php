<?php


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

class EntryService
{
    public $em;
    private $entryRepository;

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Backtest constructor.
     * @param EntityManager $em
     * @param EntryRepository $entryRepository
     */
    public function __construct($em, EntryRepository $entryRepository)
    {
        $this->em = $em;
        $this->entryRepository = $entryRepository;
    }

    /**
     * @param CandleGroup | FILOArray | Candle[] $candleGroup
     * @param $tolerance
     * @return bool
     */
    public function canGenerateEntry($candleGroup, $tolerance): bool
    {
        if (count($candleGroup)) {
            return $candleGroup->getMaxRate() >= $tolerance;
        }

        return false;
    }

    /**
     * @param CandleGroup | FILOArray | Candle[] $candleGroup
     * @param Backtest $backtest
     * @return Entry
     * @throws Exception
     */
    public function generateEntryByCandleGroupAndBacktest($candleGroup, $backtest): \Entry
    {
        /**
         * @var Candle $candle
         */
        $candle = clone $candleGroup->getLastElement();

        $candle->result = $candleGroup->getDominantResult();

        return $this->buildEntryByCandleAndBacktest($candle, $backtest);
    }

    /**
     * @param Candle $candle
     * @param Backtest $backtest
     * @return Entry
     * @throws Exception
     */
    public function buildEntryByCandleAndBacktest(Candle $candle, Backtest $backtest): \Entry
    {
        $entry = new Entry();
        $entry->pair = $candle->pair;
        $entry->backtest = $backtest;
        $entry->date = DateUtil::sumDaysToDateString($candle->getDate(), 1)->format('Y-m-d');
        $entry->time = $candle->time;
        $entry->tolerance = $backtest->tolerance;
        $entry->status = EntryStatusEnum::WAITING;
        $entry->points = $candle->points;
        $entry->interval = $backtest->interval;
        $entry->operation = $candle->getResult() > 0 ? OperationEnum::CALL : OperationEnum::PUT;
        return $entry;
    }

    /**
     * @param Entry $entry
     * @param Candle $candle
     * @return bool
     */
    public function checkEntryResult($entry, $candle): bool
    {
        return ($entry->operation === $candle->getResult());
    }

    /**
     * @param Entry $entry
     */
    public function save($entry): void
    {
        if ($entry->getIdEntry() !== null && !$this->getEm()->contains($entry)) {
            $this->getEm()->merge($entry);
        }
        $this->getEm()->persist($entry);
    }

    /**
     * @return array | EntryWatchedGroup[]
     */
    public function getGroupedWatchedEntriesByDate(): array
    {
        $sql = <<<SQL
select
    e._date as "date",
    (
       select count(*) from entry r where r._date = e._date and r._status = 2 and r.watched = true group by r._status
    ) as "waiting",
    (
        select count(*) from entry r where r._date = e._date and r._status = 1 and r.watched = true group by r._status
    ) as "rights",
    (
       select count(*) from entry r where r._date = e._date and r._status = 0 and r.watched = true group by r._status
    ) as "errors"
from entry e where e.watched = true group by e._date order by e._date DESC;
SQL;
        $con = $this->getEm()->getConnection();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, EntryWatchedGroup::class);
    }

    /**
     * @return array | Entry[]
     */
    public function getUnconfirmedWatchedEntryAsCsvByDate(DateTime $date): array
    {
        /**
         * @var Entry[] $entryList
         */
        $entryList = $this->entryRepository->findBy([
            'watched' => true,
            'date' => $date->format('Y-m-d'),
            'status' => EntryStatusEnum::WAITING
        ]);

        if (count($entryList)) {
            $arr = [];
            foreach ($entryList as $entry) {
                $arr[$entry->getPair()->getPairName() . 'OP' . $entry->operation . 'T' . $entry->time] = $entry;
            }

            $entryList = array_values($arr);
        }

        return $entryList;
    }

    /**
     * @throws OptimisticLockException
     */
    public function confirmEntries(): int
    {
        $entryRepository = $this->getEm()->getRepository(Entry::class);
        $entryList = $entryRepository->findBy([
            'status' => EntryStatusEnum::WAITING
        ]);

        $confirmed = 0;
        if(count($entryList)){
            /**
             * @var CandleRepository $candleRepository
             */
            $candleRepository = $this->getEm()->getRepository(Candle::class);
            /**
             * @var Entry $entry
             */
            foreach($entryList as $entry) {
                $candle = $candleRepository->findOneBy([
                    'pair' => $entry->getPair(),
                    'interval' => $entry->getPair()->interval,
                    'date' => $entry->date,
                    'time' => $entry->time
                ]);

                if(null === $candle) {
                    continue;
                }

                /**
                 * @var Candle $candle
                 * @var Entry $entry
                 */
                $backtest = $entry->getBacktest();
                if ($this->checkEntryResult($entry, $candle)) {
                    $entry->points = abs($candle->points);
                    $entry->status = EntryStatusEnum::SUCCESS;
                    $backtest->sumRights();
                } else {
                    $entry->points = abs($candle->points) * -1;
                    $entry->status = EntryStatusEnum::FAIL;
                    $backtest->sumErrors();
                }

                $backtest->setUpdated($entry->date . ' ' . $entry->time);
                $this->getEm()->persist($backtest);
                $this->save($entry);
                $confirmed++;
            }

            $this->getEm()->flush();
        }

        return $confirmed;
    }
}
