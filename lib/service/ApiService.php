<?php


class ApiService
{
    const KEY_FILE = 'key.f4m';

    public static function checkApplicationCredentials()
    {
        if (file_exists(self::KEY_FILE)) {
            $content = file_get_contents(self::KEY_FILE);
            $content = base64_decode($content);
            $data = json_decode($content, true);
            if (strtotime($data['expires_at']) > time()) {
                return true;
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getenv('AUTH_URL') . '/?token=' . getenv('AUTH_TOKEN'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $r = json_decode($output, true);

        if (false === ($r['status'] ?? false)) {
            return false;
        }

        if (!isset($r['expires_at'])) {
            $r['expires_at'] = (new DateTime())->add(new DateInterval('PT5H'))->format('Y-m-d H:i:s');
        }

        file_put_contents(self::KEY_FILE, base64_encode(json_encode($r)));

        return $r['status'] ?? false;
    }
}
