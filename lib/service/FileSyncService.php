<?php

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;

class FileSyncService
{
    protected $_dir;
    protected $_numberFilesInFolder;
    protected $_filesInFolder;
    protected $_numberFilesImportedToDatabase;
    protected $_numberLinesImportedToDatabase;
    protected $_dateLimitToImport;
    /**
     * @var string
     */
    private $_filesFilesFilterRegex;

    public function __construct($dir)
    {
        if (null === $dir) {
            throw new \RuntimeException('CSV dir must be specified');
        }

        if (!is_dir($dir)) {
            throw new \RuntimeException("CSV dir doesn't exist");
        }
        $this->_dir = $dir;
    }

    /**
     * @throws OptimisticLockException
     * @throws DBALException
     */
    public function sync(): void
    {
        if ($this->getNumberFilesInFolder()) {
            foreach ($this->getFilesInFolder() as $file) {
                $this->copyFileToDatabase($this->_dir.$file);
            }
        }
    }

    public function listFiles($namesOnly = false)
    {
        $list = FileTools::readDirFiles($this->_dir . '/' . $this->_filesFilesFilterRegex);

        if (false === $namesOnly) {
            return $list;
        }

        return array_map(static function ($file) {
            return FileTools::getFileName($file, true);
        }, $list);
    }

    /**
     * @param string $file Absolute path of the file
     * @throws OptimisticLockException
     * @throws DBALException
     * @throws Exception
     */
    public function copyFileToDatabase($file): void
    {
        $filename = FileTools::getFileName($file);
        $attrs = $this::getFileNameAttr($filename);

        //Verify if file exists
        $em = DB::getInstance()->getEntityManager();
        $pair = $em->getRepository('Pair')->findOneBy([
            'currencyA' => $attrs['currencyA'],
            'currencyB' => $attrs['currencyB'],
            'interval' => $attrs['interval']
        ]);

        if (!$pair) {
            $pair = new Pair();
            $pair->currencyA = $attrs['currencyA'];
            $pair->currencyB = $attrs['currencyB'];
            $pair->interval = $attrs['interval'];
            $pair->lastLineRead = 0;
            $pair->setUpdated(new \DateTime(DateUtil::OLDEST_DATE_STRING));

            $em->persist($pair);
            $em->flush();

            $this->_numberFilesImportedToDatabase++;
        }

        $file = new SplFileObject($file);

        //Point to the last line read by index
        $index = $pair->lastLineRead === 0 ? 0 : ($pair->lastLineRead - 1);
        $file->seek($index);
        $str = '';

        $i = 1;
        $totalInserted = 0;
        $line = [];
        for (; !$file->eof(); $i++) {
            $line = $file->fgetcsv();
            if (empty($line[0])) {
                continue;
            }

            $line = $this->parseLine($line);

            if (null !== $this->getDateLimitToImport()) {
                if ((new DateTime($line[0])) > $this->getDateLimitToImport()) {
                    continue;
                }
            }


            $points = $line[5] - $line[2];

            if ($points > 0) {
                $result = 1;
            } else if ($points < 0) {
                $result = -1;
            } else {
                $result = 0;
            }

            $str .= ',(
                    ' . $pair->getIdpair() . ",
                    '" . $line[0] . "',
                    '" . $line[1] . "',
                    " . $pair->interval . ',
                    ' . $line[2] . ',
                    ' . $line[3] . ',
                    ' . $line[4] . ',
                    ' . $line[5] . ',
                    ' . $line[6] . ',
                    ' . round(($line[5] - $line[2]), 5) . ',
                    ' . $result . '
                )';
            $pair->lastLineRead++;
            $totalInserted++;

            if (100 === $i) {
                $i = 1;
                $this->bulkInsert($str);
                $str = '';
            }
        }

        if (!empty($str) && $i < 3000) {
            $this->bulkInsert($str);
        }

        if ($totalInserted > 0) {
            $pair->setUpdated(new DateTime($line[0] . ' ' . $line[1]));
        }

        $em->persist($pair);
        $em->flush();
        $this->_numberLinesImportedToDatabase = $totalInserted;
    }

    /**
     * @param $str
     * @throws DBALException
     */
    public function bulkInsert($str): void
    {
        if (strpos($str, ',') === 0) {
            $str = substr($str, 1);
        }

        $sql = '
                INSERT INTO 
                    candle (pair_idpair, _date, _time, _interval, _open, _max, _min, _close, volume, points, result) 
                VALUES ' . $str;

        $em = DB::getInstance()->getEntityManager();
        $em->getConnection()->executeQuery($sql);
    }

    public static function getFileNameAttr($filename): ?array
    {
        $filename = trim($filename);
        return [
            'currencyA' => substr($filename, 0, 3),
            'currencyB' => substr($filename, 3, 3),
            'interval' => preg_replace('/[^0-9]/', '', $filename)
        ];
    }

    /**
     * @return int
     */
    public function getNumberFilesInFolder(): ?int
    {
        if (!isset($this->_numberFilesInFolder)) {
            $this->setNumberFilesInFolder(count($this->getFilesInFolder()));
        }
        return $this->_numberFilesInFolder;
    }

    /**
     * @param int $filesInFolder
     */
    public function setNumberFilesInFolder($filesInFolder): void
    {
        $this->_numberFilesInFolder = $filesInFolder;
    }

    /**
     * @return int
     */
    public function getNumberFilesImportedToDatabase(): ?int
    {
        return $this->_numberFilesImportedToDatabase;
    }

    /**
     * @param int $filesImportedToDatabase
     */
    public function setNumberFilesImportedToDatabase($filesImportedToDatabase): void
    {
        $this->_numberFilesImportedToDatabase = $filesImportedToDatabase;
    }

    /**
     * @return int
     */
    public function getNumberLinesImportedToDatabase(): ?int
    {
        return $this->_numberLinesImportedToDatabase;
    }

    /**
     * @param int $linesImportedToDatabase
     */
    public function setNumberLinesImportedToDatabase($linesImportedToDatabase): void
    {
        $this->_numberLinesImportedToDatabase = $linesImportedToDatabase;
    }

    /**
     * @return string
     */
    public function getSyncResultString(): ?string
    {
        $r = [];
        if ($this->getNumberFilesInFolder()) {
            $r[] = TextTools::plural($this->getNumberFilesInFolder(), "arquivo", "arquivos", "Nenhum arquivo") . " na pasta";
        }

        if ($this->getNumberFilesImportedToDatabase()) {
            $r[] = TextTools::plural($this->getNumberFilesImportedToDatabase(), "arquivo importado", "arquivos importados", "Nenhum arquivo importado");
        }

        if ($this->getNumberLinesImportedToDatabase()) {
            $r[] = TextTools::plural($this->getNumberLinesImportedToDatabase(), "vela adicionada", "velas adicionadas", "Nenhuma vela adicionada");
        }

        if (count($r) === 0) {
            $r[] = 'Nenhuma ação a ser realizada';
        }

        return implode('. ', $r);
    }

    /**
     * @return array
     */
    public function getFilesInFolder(): ?array
    {
        if (!$this->_filesInFolder) {
            $this->setFilesInFolder($this->listFiles());
        }
        return $this->_filesInFolder;
    }

    /**
     * @param mixed $filesInFolder
     */
    public function setFilesInFolder($filesInFolder): void
    {
        $this->_filesInFolder = $filesInFolder;
    }

    /**
     * @return DateTime | null
     */
    public function getDateLimitToImport(): ?\DateTime
    {
        return $this->_dateLimitToImport;
    }

    /**
     * @param DateTime $limitDate
     */
    public function setDateLimitToImport($limitDate): void
    {
        $this->_dateLimitToImport = $limitDate;
    }

    public function parseLine(array $line)
    {
        $lineRegisters = count($line);

        $tmp = [];

        $i = 0;

        $line[0] = str_replace('.', '-', $line[0]);

        if (strlen($line[0]) === 16) {
            $date = explode(' ', $line[0]);
            $tmp[] = $date[0];
            $tmp[] = $date[1];
            $i = 1;
        }

        while ($i < $lineRegisters) {
            $tmp[] = $line[$i++];
        }

        if (strlen($tmp[1]) === 5) {
            $tmp[1] .= ':00';
        }

        return $tmp;
    }

    /**
     * @param string $string
     */
    public function setFilesFilterRegex(string $string)
    {
        $this->_filesFilesFilterRegex = $string;
    }
}
