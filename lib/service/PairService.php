<?php


use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

class PairService
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param Pair $pair
     * @param bool $first
     * @return DateTime | null
     */
    public static function getFirstOrLastCandleDatetime(Pair $pair, $first = true): ?\DateTime
    {
        $em = DB::getInstance()->getEntityManager();
        $candleRepository = $em->getRepository(Candle::class);

        /**
         * @var Candle $candle
         */
        $candle = $candleRepository->findOneBy([
            'pair' => $pair
        ],
            [
                'date' => $first ? 'DESC' : 'ASC'
            ]);

        if (null === $candle) {
            return null;
        }

        try {
            return $candle->getDate();
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param Pair $pair
     * @return string
     */
    public static function getTotalCandlesByPair(Pair $pair): string
    {
        /**
         * @var CandleRepository $candleRepository
         */
        $candleRepository = DB::getInstance()->getEntityManager()->getRepository(Candle::class);
        try {
            return $candleRepository->getTotalCandlesByPair($pair);
        } catch (DBALException $e) {
            return 0;
        }
    }

    /**
     * @param Pair $pair
     * @throws DBALException|OptimisticLockException
     */
    public function remove($pair): void
    {
        $sql = 'DELETE FROM entry WHERE pair_idpair = :idpair';
        $this->em->getConnection()->executeQuery($sql, ['idpair' => $pair->getIdpair()]);

        $sql = 'DELETE FROM backtest WHERE pair_idpair = :idpair';
        $this->em->getConnection()->executeQuery($sql, ['idpair' => $pair->getIdpair()]);

        $sql = 'DELETE FROM candle WHERE pair_idpair = :idpair';
        $this->em->getConnection()->executeQuery($sql, ['idpair' => $pair->getIdpair()]);

        $this->em->remove($pair);
        $this->em->flush();
    }
}
