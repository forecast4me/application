<?php


class BacktestFormatter
{
    /**
     * @param Backtest $backtest
     * @return mixed|string
     * @throws Exception
     */
    public static function getTime(Backtest $backtest){
        return DateFormatter::parseTime($backtest->time);
    }

    /**
     * @param Backtest $backtest
     * @return string
     */
    public static function getSuccessRatio(Backtest $backtest): string
    {
        return ($backtest->calcSuccessRatio() * 100).'%';
    }
}
