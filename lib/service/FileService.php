<?php

class FileService
{
    protected $_dir;

    public function __construct($dir)
    {
        $this->_dir = $dir;
    }

    /**
     * @param $file
     * @param $destName
     */
    public function saveFile($file, $destName)
    {
        $path = $this->_dir . $destName;
        if (file_exists($path)) {
            $this->removeFile($destName);
        }

        if (!move_uploaded_file($file, $path)) {
            throw new \RuntimeException('Falha ao realizar upload do arquivo ' . $destName);
        }
    }

    /**
     * @param $file
     * @return bool
     */
    public function removeFile($file): bool
    {
        return unlink($this->_dir . $file);
    }

    public function transformPostedFilesInArray($files): ?array
    {
        $result = array();
        foreach ($files as $key1 => $value1) {
            foreach ($value1 as $key2 => $value2) {
                $result[$key2][$key1] = $value2;
            }
        }
        return $result;
    }
}
