<?php
	/**
	 * @var HtmlOutput $this
	 */
?>
<base href="<?=URL::getBaseURL()?>">

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<script src="<?=URL::getURL('assets/js/jquery.js')?>"></script>

<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<title><?=$this->getTitle()?></title>

<script src="<?=URL::getURL('assets/plugins/jquery-toast-plugin-master/dist/jquery.toast.min.js')?>"></script>
<link href="<?=URL::getURL('assets/plugins/jquery-toast-plugin-master/dist/jquery.toast.min.css')?>" rel="stylesheet" />

<link href="<?=URL::getURL('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css')?>" rel="stylesheet">
<script src="<?=URL::getURL('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js')?>"></script>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<script>
    window['backend_url'] = '<?=getenv('BASE_URL')?>';
	$(document).ready(function(){
		$(".datepicker").datepicker({ dateFormat: 'dd/mm/yy' });
		$('[data-toggle="tooltip"]').tooltip();

		<?php
			$_sys_notifications = SystemNotificator::getInstance()->getNotifications();
			if(count($_sys_notifications)):
				foreach($_sys_notifications as $notification):
		?>
		$.toast({
			heading: '<?=$notification->getTypeString()?>',
			text: '<?=$notification->message?>',
			icon: '<?=$notification->getIconString()?>',
			hideAfter: false
		});
		<?php
				endforeach;
				SystemNotificator::getInstance()->clearAll();
			endif;
		?>
	});
</script>

<link href="<?=URL::getURL('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<script src="<?=URL::getURL('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<link href="<?=URL::getURL('assets/css/main.css')?>" rel="stylesheet">
