<?php

use Doctrine\ORM\OptimisticLockException;

/**
 * @Route("/CallTrade", name="calltrade_")
 */
class CallTradeController
{
    protected $em;
    private $_title = 'Plataforma Forecast 4 me - ';

    public function __construct()
    {
        $this->em = DB::getInstance()->getEntityManager();
    }

    /**
     * @Route("/index", name="backtest_view")
     * @return HtmlOutput
     */
    public function indexAction(): \HtmlOutput
    {
        $templateVars = [];

        $pairRepository = $this->em->getRepository('Pair');
        $pairs = $pairRepository->findAll();
        $templateVars['pairs'] = $pairs;

        $idPair = $_SESSION['idpair'] ?? null;
        $days = $_SESSION['days'] ?? null;
        $tolerance = $_SESSION['tolerance'] ?? null;
        $minRightsRatio = $_SESSION['minRightsRatio'] ?? null;

        try {
            if (
                (null !== $idPair) &&
                (null !== $days) &&
                (null !== $tolerance)
            ) {
                /**
                 * @var Pair $pair
                 */
                $pair = $pairRepository->find($_SESSION['idpair']);
                if (null === $pair) {
                    $_SESSION['idpair'] = $_SESSION['days'] = $_SESSION['tolerance'] = $_SESSION['minRightsRatio'] = null;
                    throw new \RuntimeException('O par selecionado não está mais disponível');
                }

                $entries = $this->em->getRepository('Entry')->findByLatestEntriesByPairIntervalTolerance($pair, $days, $tolerance / 100);

                if (null !== $minRightsRatio) {
                    $entries = array_filter($entries, static function ($el) use ($minRightsRatio) {
                        /**
                         * @var Entry $el
                         */
                        return $el->getBacktest()->calcSuccessRatio() >= ($minRightsRatio / 100);
                    });
                    $entries = array_values($entries);
                }

                if (count($entries) > 1) {
                    usort($entries, static function ($a, $b) {
                        /**
                         * @var Entry $a
                         * @var Entry $b
                         */

                        return $a->getBacktest()->calcSuccessRatio() <= $b->getBacktest()->calcSuccessRatio();
                    });
                }

                $backtests = $this->em->getRepository('Backtest')->findBy([
                    'pair' => $pair,
                    'interval' => $days,
                    'tolerance' => $tolerance / 100
                ]);

                $templateVars['idpair'] = $idPair;
                $templateVars['days'] = $days;
                $templateVars['tolerance'] = $tolerance;
                $templateVars['minRightsRatio'] = $minRightsRatio;
                $templateVars['entries'] = $entries;
                $templateVars['backtests'] = $backtests;
            }
        } catch (Exception $e) {
            SystemNotificator::getInstance()->addNotification($e->getMessage(), ISystemNotificationType::ERROR);
        }


        return new HtmlOutput('CallTrade/dashboard', $this->_title . 'Dashboard', $templateVars);
    }

    /**
     * @Route("/doBacktest", name="backtest_do", methods={"POST"})
     * @return IOutput
     */
    public function postDoBacktestAction(): \IOutput
    {
        $_SESSION['minRightsRatio'] = $_POST['minRightsRatio'];

        $idpair = $_SESSION['idpair'] = (int)$_POST['idpair'];
        $days = $_SESSION['days'] = $_POST['days'];
        $tolerance = $_SESSION['tolerance'] = $_POST['tolerance'];
        $tolerance /= 100;


        $r['status'] = true;

        $em = DB::getInstance()->getEntityManager();

        try {
            $em->beginTransaction();

            /**
             * @var EntryRepository $entryRepository
             */
            $entryRepository = $em->getRepository(Entry::class);
            $bs = new BacktestService($em, new EntryService($em, $entryRepository));
            $bs->start($idpair, $days, $tolerance);
            SystemNotificator::getInstance()->addNotification(
                'Operação realizada com sucesso',
                ISystemNotificationType::SUCCESS
            );

            $em->commit();
        } catch (Exception $e) {
            SystemNotificator::getInstance()->addNotification(
                $e->getMessage(),
                ISystemNotificationType::ERROR
            );
            exit();
        }

        return new RedirectOutput('CallTrade/');
    }

    /**
     * @Route("/syncFiles", name="files_sync")
     * @return IOutput
     */
    public function syncFilesAction(): \IOutput
    {
        try {
            $dateLimitToImport = $_GET['dateLimitToImport'] ?? null;
            DB::getInstance()->getEntityManager()->beginTransaction();
            $fileSync = new FileSyncService(getenv('CSV_PATH'));
            $fileSync->setFilesFilterRegex('/*.csv');

            if (null !== $dateLimitToImport) {
                $dateLimitToImport = DateUtil::dateConvert($dateLimitToImport);
                $fileSync->setDateLimitToImport(new DateTime($dateLimitToImport));
            }

            $fileSync->sync();
            DB::getInstance()->getEntityManager()->commit();

            SystemNotificator::getInstance()->addNotification(
                'Processamento realizado com sucesso. ' . $fileSync->getSyncResultString(),
                ISystemNotificationType::SUCCESS
            );
        } catch (\Throwable $e) {
            DB::getInstance()->getEntityManager()->rollBack();
            SystemNotificator::getInstance()->addNotification(
                $e->getMessage(),
                ISystemNotificationType::ERROR
            );
        }

        return new RedirectOutput('CallTrade/confirmEntries');
    }

    /**
     * @Route("/uploadFiles", name="files_upload_view")
     * @return IOutput
     */
    public function uploadFilesAction(): \IOutput
    {
        return new HtmlOutput('CallTrade/file-manager/upload-form.tpl', $this->_title . 'Uploads');
    }

    /**
     * @Route("/uploadFiles", name="files_upload_do", methods={"POST"})
     * @return IOutput
     */
    public function postUploadFilesAction(): \IOutput
    {
        if (count($_FILES) === 0) {
            SystemNotificator::getInstance()->addNotification('Nenhum arquivo selecionado', ISystemNotificationType::ALERT);
            return new RedirectOutput('CallTrade/uploadFiles');
        }

        $fileService = new FileService(getenv('CSV_PATH'));
        $files = $fileService->transformPostedFilesInArray($_FILES['file']);

        foreach ($files as $file) {
            $fileService->saveFile($file['tmp_name'], $file['name']);
        }

        $limitDateToImport = $_POST['dateLimitToImport'] ?? null;
        if (null !== $limitDateToImport) {
            return new RedirectOutput('CallTrade/syncFiles/?dateLimitToImport=' . $limitDateToImport);
        }

        return new RedirectOutput('CallTrade/syncFiles');
    }

    /**
     * @Route("/managePairs", name="pair_manage")
     * @return IOutput
     */
    public function managePairsAction(): \IOutput
    {
        $pairRepository = $this->em->getRepository(Pair::class);
        return new HtmlOutput(
            'CallTrade/pair-manager/manage-form.tpl',
            'Gerenciar moedas',
            [
                'list' => $pairRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/removePair", name="pair_remove", methods={"POST"})
     * @return IOutput
     */
    public function postRemovePairAction(): \IOutput
    {
        $pairId = $_POST['pairId'];

        $pairRepository = $this->em->getRepository('Pair');
        /**
         * @var Pair $pair
         */
        $pair = $pairRepository->findOneBy(['idpair' => $pairId]);

        try {
            if (null === $pair) {
                throw new \RuntimeException('Par de moeda inválido');
            }

            $this->em->beginTransaction();

            $pairService = new PairService($this->em);
            $pairService->remove($pair);

            $fileService = new FileService(getenv('CSV_PATH'));
            $fileService->removeFile($pair->getFileName());

            $this->em->commit();
            return new JsonOutput(['status' => true]);
        } catch (Exception $e) {
            return new JsonOutput(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    public function configAction(): \IOutput
    {
        $data = [
            'timezoneDiff' => DateUtil::getTimezoneDiff()
        ];

        return new HtmlOutput('CallTrade/config/form.tpl', 'Configurações', $data);
    }

    /**
     * @Route("/entryWacher", name="entry_wacther", methods={"GET"})
     * @return IOutput
     */
    public function entryWatcherAction(): \IOutput
    {
        return new HtmlOutput(
            'CallTrade/entry-watcher/main.tpl',
            'Monitor de entradas'
        );
    }

    /**
     * @Route("CallTrade/entryWatcherExport/{date}", name="entry_watcher_export", methods={"GET"})
     * @throws Exception
     */
    public function getUnconfirmedWatchedEntryAsCsvByDateAction()
    {
        /**
         * @var Request $request
         */
        $request = SingletonManager::getSingleton('Request');
        $dateTime = new DateTime($request->get('date'));

        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->em->getRepository(Entry::class);
        $entryService = new EntryService($this->em, $entryRepository);

        $entryList = $entryService->getUnconfirmedWatchedEntryAsCsvByDate($dateTime);

        $str = '';
        if (count($entryList)) {
            foreach ($entryList as $entry) {
                $str .= $entry->getPair()->currencyA . $entry->getPair()->currencyB . '.pbo';
                $str .= "\t" . $entry->time;
                $str .= "\t" . $entry->getPair()->interval;
                $str .= "\t" . EntryFormatter::getOperation($entry);
                $str .= "\n";
            }

            return new FileOutput([
                'filename' => 'entries_' . $dateTime->format('Y-m-d') . '.csv',
                'content' => $str
            ]);
        }

        return new JsonOutput();
    }

    /**
     * @Route("CallTrade/confirmEntries", name="entry_confirm", methods={"GET"})
     * @throws OptimisticLockException
     */
    public function confirmEntriesAction(): IOutput
    {
        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->em->getRepository(Entry::class);
        $entryService = new EntryService($this->em, $entryRepository);
        $confirmed = $entryService->confirmEntries();

        $type = ($confirmed > 0) ? ISystemNotificationType::SUCCESS : ISystemNotificationType::NOTICE;
        SystemNotificator::getInstance()->addNotification(
            TextTools::plural($confirmed, 'Uma entrada confirmada', 'entradas confirmadas', 'Nenhuma nova confirmação', true),
            $type
        );

        return new RedirectOutput('CallTrade/');
    }

    /**
     *
     */
    public function postConfigAction(): \IOutput
    {
        $timezoneDiff = Tools::ai($_POST['timezoneDiff'] ?? -3);
        if(
            is_numeric($_POST['timezoneDiff']) &&
            $_POST['timezoneDiff'] > -12 &&
            $_POST['timezoneDiff'] < 12
        ) {
            DateUtil::setTimezoneDiff($timezoneDiff);
        }

        SystemNotificator::getInstance()->addNotification('Configurações atualizadas', ISystemNotificationType::SUCCESS);
        return new RedirectOutput('CallTrade/config');
    }
}
