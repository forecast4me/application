<?php

class HomeController
{
    private $_title = 'Plataforma Trader O.B. - ';

    public function indexAction(): \IOutput
    {
        return new RedirectOutput('/CallTrade');
    }
}
