<?php

use Doctrine\ORM\EntityManager;

/**
 * Class CallTradeApiController
 */
class CallTradeApiController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * CallTradeApiController constructor.
     */
    public function __construct()
    {
        $this->em = DB::getInstance()->getEntityManager();
    }

    /**
     * @return JsonOutput
     */
    public function getWatchedEntriesGroupsAction(): \JsonOutput
    {
        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->em->getRepository(Entry::class);
        $entryService = new EntryService($this->em, $entryRepository);
        return new JsonOutput($entryService->getGroupedWatchedEntriesByDate());
    }

    /**
     * @return JsonOutput
     * @throws Exception
     */
    public function postGetWatchedEntriesByDateTimeAction(): \JsonOutput
    {
        /**
         * @var Request $request
         */
        $request = SingletonManager::getSingleton(Request::class);
        $data = $request->getContentParsedAsJson();

        $fromDateTime = null;
        $toDateTime = null;

        if (isset($data->from)) {
            $fromDateTime = new DateTime($data->from);
        }

        if (isset($data->to)) {
            $toDateTime = new DateTime($data->to);
        }

        /**
         * @var EntryRepository $entryRepository
         */
        $entryRepository = $this->em->getRepository(Entry::class);

        return new JsonOutput($entryRepository->findByDate($fromDateTime, $toDateTime, true));
    }

    /**
     *
     * @throws Exception
     */
    public function postWatchEntryAction(): ?\JsonOutput
    {
        try {
            /**
             * @var Request $request
             */
            $request = SingletonManager::getSingleton(Request::class);
            $data = $request->getContentParsedAsJson();
            $entryId = $data->entryId ?? null;

            if(null === $entryId){
                throw new RuntimeException('Entity id not found');
            }

            /**
             * @var EntryRepository $entryRepository
             */
            $entryRepository = $this->em->getRepository(Entry::class);

            /**
             * @var Entry $entry
             */
            $entry = $entryRepository->find($entryId);

            if (null === $entry) {
                throw new RuntimeException('Entry not found');
            }

            $entry->setWatched(!$entry->getWatched());

            $entryRepository->save($entry);

            $this->em->flush();

            return new JsonOutput([
                'status' => true,
                'current' => $entry->getWatched(),
                'entry' => $entry
            ]);
        } catch (RuntimeException $e) {
            return new JsonOutput([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
