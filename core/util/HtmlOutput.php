<?php
class HtmlOutput implements IOutput {
	public $templateName, $title, $templateVars;

	/**
	 * @param string $templateName
	 */
	public function setTemplateName($templateName) {
		$this->templateName = $templateName;
	}

	/**
	 * @return string
	 */
	public function getTemplateName() {
		return $this->templateName;
	}

	/**
	 * @param array $templateVars
	 */
	public function setTemplateVars($templateVars) {
		$this->templateVars = $templateVars;
	}

	/**
	 * @return array
	 */
	public function getTemplateVars() {
		return $this->templateVars;
	}

	public function addTemplateVar($k, $v){
		$this->templateVars[$k] = $v;
	}

	public function removeTemplateVar($k){
		unset($this->templateVars[$k]);
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $htmlContent
	 */
	public function setHtmlContent($htmlContent) {
		$this->_htmlContent = $htmlContent;
	}

	/**
	 * @return string
	 */
	public function getHtmlContent() {
		return $this->_htmlContent;
	}

	protected $_htmlContent;

	public function __construct($templateName, $title = NULL, $templateVars = null){
		$this->setTemplateName($templateName);
		$this->setTitle($title);
		$this->setTemplateVars($templateVars);
	}

	public function render(){
		if(!file_exists($this->getTemplateFilePath())){
			$this->setTemplateName("Errors/404");
		}

		ob_start();

		if($this->templateVars){
			extract($this->templateVars);
		}

		include $this->getTemplateFilePath();

		$this->setHtmlContent(ob_get_clean());
		return $this->getHtmlContent();
	}

	private function getTemplateFilePath(){
		return "lib/view/".$this->templateName.".php";
	}

	public function __toString(){
		return $this->render();
	}
}
