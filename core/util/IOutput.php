<?php
interface IOutput{
    public function render();
    public function __toString();
}