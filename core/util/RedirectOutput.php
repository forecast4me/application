<?php
/**
 * Código desenvolvido por Renan
 * Criado em: 12/05/17 às 19:04
 * Todos os códigos deste projeto são de propriedade do autor, mas com licença de uso ao cliente
 */

class RedirectOutput implements IOutput{
	private $_url, $_useBaseURL;
	public function __construct($url, $useBaseURL = true){
		$this->setUrl($url);
		$this->setUseBaseURL($useBaseURL);
	}

	public function render(){
        if (ob_get_contents()) ob_end_clean();

		if($this->getUseBaseURL()){
			$this->setUrl(URL::getBaseURL().$this->getUrl());
		}

		header("Location: ".$this->getUrl());
		exit();
	}

	public function __toString(){
		$this->render();
		return '';
	}

	/**
	 * @return mixed
	 */
	public function getUrl() {
		return $this->_url;
	}

	/**
	 * @param mixed $url
	 */
	public function setUrl($url) {
		$this->_url = $url;
	}

	/**
	 * @return mixed
	 */
	public function getUseBaseURL() {
		return $this->_useBaseURL;
	}

	/**
	 * @param mixed $useBaseURL
	 */
	public function setUseBaseURL($useBaseURL) {
		$this->_useBaseURL = $useBaseURL;
	}
}
