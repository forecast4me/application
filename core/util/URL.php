<?php
	class URL{
		const FRIENDLY_URL = 1;
		public $_baseUrl = "";
		private $_originalGet;

		//MÉTODO DE CONEXÃO PADRÃO
		private static $instance;
		private function __construct(){
			$this->_baseUrl = "";
		}
		/**
		 * @return URL
		 */
		public static function getInstance(){
			if (!isset(self::$instance)) {
				$c = __CLASS__;
				self::$instance = new $c;
			}

			return self::$instance;
		}

		public static function map($arrParams = []){
			if(!self::getInstance()->_originalGet){
				self::setOriginalGet($_GET);
			}
			if(!sizeof($arrParams)){
				$arrParams = array("Area", "action", "ID");
			}
			$parameters = self::getURLParameters();
			if(self::FRIENDLY_URL){
				$i = 0;
				foreach($arrParams as $param){
					$_GET[$param] = $parameters[$i++] ?? null;
				}
			}
		}
		public static function get($param){
			return $_GET[$param];
		}
		public static function set($param, $value){
			$_GET[$param] = $value;
		}

		public static function getOriginalGet(){
			return self::getInstance()->_originalGet;
		}
		public static function setOriginalGet($arr){
			self::getInstance()->_originalGet = $arr;
		}

		public static function getURLParameters(){
			$requestURI = $_SERVER['REQUEST_URI'];
			if(self::getPath() != "/")
				$url = str_replace(self::getPath(), "", $requestURI);
			else
				$url = $requestURI;
			if(substr_count($url, "/")){
				$parameters = explode("/", $url);
				$parameters = array_filter($parameters);
				$parameters = array_values($parameters);
			}
			return $parameters;
		}
		public static function getPath(){
			$burl = self::getInstance()->_baseUrl;
			return str_replace(array("http://", $_SERVER["HTTP_HOST"]), "", $burl);
		}
		public static function getURL($parameters = NULL, $url = NULL){
			if(is_array($parameters)){
				if(sizeof($parameters)){
					if(self::FRIENDLY_URL == 1){
						$parameters = implode("/",$parameters)."/";
					}else{
						foreach($parameters as $k=>$v){
							$url[]= $k."=".$v;
						}
						$parameters = "?".implode("&",$url);
					}
				}
			}
			return $url.$parameters;
		}

		public static function make($area, $action){
		    return self::getURL([$area, $action], self::getBaseURL());
        }

		public static function getDomain(){
			$parts = explode("/", self::getInstance()->_baseUrl);
			$npart[] = $parts[0];
			$npart[] = $parts[1];
			$npart[] = $parts[2];
			return implode("/", $npart);
		}

		public static function getBaseURL(){
			return self::getInstance()->_baseUrl;
		}
		public static function setBaseURL($string) {
			self::getInstance()->_baseUrl = $string;
		}
	}
