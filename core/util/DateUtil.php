<?php

class DateUtil
{
    const OLDEST_DATE_STRING = '1980-01-01 00:00:00';

    public static function dateConvert($date, $horas = false)
    {
        if (!$date) return;
        if (strlen($date) > 10 && strstr($date, "-")) {
            $date = explode(" ", $date);
            $time = $date[1];
            $date = $date[0];
        }

        if (strpos($date, "/") !== false) {
            $date = explode("/", $date);
            $date = $date[2] . "-" . $date[1] . "-" . $date[0];
        } else {
            $date = explode("-", $date);
            $date = $date[2] . "/" . $date[1] . "/" . $date[0];
        }

        if ($horas && $time) {
            $date .= " às $time";
        }
        return $date;
    }

    /**
     * @return mixed|null
     * @codeCoverageIgnore
     */
    public static function getTimezoneDiff()
    {
        /**
         * @var CookieService $cookieService
         */
        $cookieService = CookieService::getInstance();
        return $cookieService->get('timezoneDiff');
    }

    /**
     * @param $timezoneDiff
     * @codeCoverageIgnore
     */
    public static function setTimezoneDiff($timezoneDiff): void
    {
        /**
         * @var CookieService $cookieService
         */
        $cookieService = CookieService::getInstance();
        $cookieService->set('timezoneDiff', $timezoneDiff);
    }

    /**
     * @param DateTime | string | null $datetime
     * @param int $offset
     * @return mixed
     * @throws Exception
     */
    public static function normalize($datetime, $offset = 0)
    {
        if ($datetime === null) {
            return null;
        }

        if (is_string($datetime)) {
            $datetime = new DateTime($datetime);
        }

        if ($offset < 0) {
            $h = $offset * -1;
            $interval = new DateInterval('PT' . $h . 'H');
            return $datetime->sub($interval);
        }

        if ($offset > 0) {
            $h = $offset;
            $interval = new DateInterval('PT' . $h . 'H');
            return $datetime->add($interval);
        }

        return $datetime;
    }

    /**
     * @param DateTimeInterface $date
     * @param $days
     * @return DateTime
     * @throws Exception
     */
    public static function sumDaysToDateString($date, $days): \DateTime
    {
        $operation = ($days < 0) ? 'sub' : 'sum';
        if ($operation === 'sub') {
            $days *= -1;
        }

        $weekends = 0;
        $weekday = (int)$date->format('N');

        if ($operation === 'sub') {
            $arr = [6, 5, 4, 3, 2, 1, 0];
            $weekday = $arr[$weekday];
        }

        $totalDays = $days + $weekday;

        $weekends += floor($totalDays / 5);

        if ($totalDays % 5 === 0) {
            $weekends--;
        }

        $daysToJump = $days + ($weekends * 2);

        $interval = new DateInterval('P' . $daysToJump . 'D');

        $date = new DateTime($date->format('Y-m-d H:i:s'));
        return ($operation === 'sub') ? $date->sub($interval) : $date->add($interval);
    }
}
