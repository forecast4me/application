<?php
interface ISystemNotificationType {
	const
		ERROR = 0,
		SUCCESS = 1,
		ALERT = 2,
		NOTICE = 3;
}