<?php
class DBPaginatorManager {
	private $_paginationKey;
	private $_paginatorCollection;

	/**
	 * @return mixed
	 */
	public function getPaginationKey() {
		return $this->_paginationKey;
	}

	/**
	 * @param mixed $paginationKey
	 */
	public function setPaginationKey($paginationKey = NULL) {
		if($paginationKey != NULL){
			$this->_paginationKey = md5($paginationKey);
		}else{
			unset($this->_paginationKey);
		}
	}

	//MÉTODO DE CONEXÃO PADRÃO
	private static $instance;
	private function __construct(){}
	/**
	 * @return DBPaginatorManager
	 */
	public static function getInstance(){
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}

	/**
	 * @param null $k
	 * @return DBPaginator mixed
	 */
	public function getPaginator($k = NULL){
		if($k != NULL){
			$this->setPaginationKey($k);
		}
		if($this->getPaginationKey() != NULL){
			$r = $this->_paginatorCollection[$this->getPaginationKey()];
			$this->setPaginationKey(NULL);
			return $r;
		}
		return NULL;
	}

	/**
	 * @param DBPaginator $dbp
	 */
	public function addPaginator($dbp){
		DBPaginatorManager::getInstance()->setPaginationKey($dbp->getPageUrlParam());
		$this->_paginatorCollection[$this->getPaginationKey()] = $dbp;
	}
} 