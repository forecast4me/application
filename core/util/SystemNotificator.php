<?php

class SystemNotificator
{
    /**
     * @var SystemNotificator
     */
    protected static $instance;
    private $notifications;
    private $initialized = false;
    const SESSION_KEY = '_system_notifications';

    protected function __construct()
    {
    }

    /**
     * @return SystemNotificator
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }

        self::$instance->getNotifications();

        return self::$instance;
    }

    public function addNotification($message, $type = 0)
    {
        $this->notifications[] = new SystemNotification($message, $type);
    }

    /**
     * @return SystemNotification[]
     */
    public function getNotifications()
    {
        if (!isset($this->notifications) && !$this->initialized) {
            $this->initialized = true;
            $this->notifications = $_SESSION[self::SESSION_KEY];
        }

        return $this->notifications;
    }

    public function clearAll()
    {
        return $this->notifications = null;
    }

    public function __toString()
    {
        $str = '';
        if (count($this->getNotifications())) {
            foreach ($this->getNotifications() as $notification) {
                $str .= $notification->getTypeString() . " - " . $notification->message;
            }
        }

        return $str;
    }

    public function __destruct()
    {
        $_SESSION[self::SESSION_KEY] = $this->notifications;
    }
}
