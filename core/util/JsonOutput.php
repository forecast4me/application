<?php
class JsonOutput implements IOutput {
	public $vars;

	/**
	 * @param array $vars
	 */
	public function setVars($vars) {
		$this->vars = $vars;
	}

	/**
	 * @return array
	 */
	public function getVars() {
		return $this->vars;
	}

	public function addVar($k, $v){
		$this->vars[$k] = $v;
	}

	public function removeVar($k){
		unset($this->vars[$k]);
	}

	public function __construct($vars = null){
		$this->setVars($vars);
	}

	public function render(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Content-Type");
		header('Content-Type: application/json');
		return json_encode($this->getVars());
	}

	public function __toString(){
		return $this->render();
	}
}
