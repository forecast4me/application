<?php
    /**
     * Created by PhpStorm.
     * User: Renan
     * Date: 23/03/2019
     * Time: 22:35
     */

    class CachedOutput implements IOutput {
        const CACHE_PATH = "cache";
        /**
         * @var IOutput $output
         */
        protected $key, $output, $html;

        public function __construct($key = "") {
            $this->key = $key;
        }

        /**
         * @param IOutput $output
         */
        public function setOutput($output){
            $this->output = $output;
        }

        public function exists(){
            //CHECK IF FILE EXISTS
            return file_exists($this->getCachedFileURI());
        }

        public function render(){
            if($this->output && !ENABLE_CACHE){
                return $this->output->render();
            }

            if(!$this->exists()){
                if($this->output){
                    $this->html = $this->output->render();
                    $this->write();
                }else{
                    return false;
                }
            }

            return $this->read();
        }

        protected function getCachedFileURI(){
            return self::CACHE_PATH.DIRECTORY_SEPARATOR.$this->key.".cache";
        }

        protected function read(){
            //IF HTML IS SET, RETURN
            if($this->html){
                return $this->html;
            }

            //READ THE CACHED FILE
            if(file_exists($this->getCachedFileURI())){
                return file_get_contents($this->getCachedFileURI());
            }

            return "";
        }

        protected function write(){
            $this->checkDestinationDir(self::CACHE_PATH);
            //SAVE THE OUTPUT CONTENT TO A FILE
            file_put_contents($this->getCachedFileURI(), $this->html);
        }

        protected function checkDestinationDir($dir){
            //IF DIR DOESN'T EXISTS, CREATE
            if(!file_exists($dir)){
                FileTools::createDir($dir);
            }
        }

        public function __toString(){
            return $this->render();
        }
    }
