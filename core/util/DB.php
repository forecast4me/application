<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class DB
{
    var $con;
    var $host;
    var $user;
    var $pass;
    var $db;
    var $status;

    var $_pdoInstance;
    var $sqlQuery;
    var $queryList;
    private $_dbPaginatorManager;
    protected $_entityManager;

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->_entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->_entityManager = $entityManager;
    }

    /**
     * @return DBPaginatorManager
     */
    public function getDbPaginatorManager()
    {
        return $this->_dbPaginatorManager;
    }

    /**
     * @param mixed $dbPaginatorManager
     */
    public function setDbPaginatorManager($dbPaginatorManager)
    {
        $this->_dbPaginatorManager = $dbPaginatorManager;
    }

    //MÉTODO DE CONEXÃO PADRÃO
    private static $instance;

    private function __construct()
    {
        $this->_dbPaginatorManager = DBPaginatorManager::getInstance();
    }

    /**
     * @return DB
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    public static function connect(): bool
    {
        $i = self::getInstance();
        try {
            $i->setPDO(
                new PDO(
                    'mysql:host=' . $i->host . ';port=3306;dbname=' . $i->db . ';charset=UTF8',
                    $i->user,
                    $i->pass,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)
                )
            );
            $pdo = $i->getPDO();

            $pdo->exec('SET NAMES utf8;');

            $paths = array(ROOT_PATH . '/lib/entity');
            $isDevMode = false;
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
            $config->setAutoGenerateProxyClasses(true);
            $entityManager = EntityManager::create(['pdo' => $pdo], $config);
            $i->setEntityManager($entityManager);
        } catch (\Throwable $e) {
            SystemNotificator::getInstance()->addNotification($e->getMessage(), $e->getCode());
            return false;
        }

        return true;
    }

    /**
     * Controla as transações com banco de dados<br>
     * Parâmetros aceitos: start, commit e rollback
     * @param string $action
     */
    public static function transaction($action = 'start'): void
    {
        switch ($action) {
            case 'start':
                self::getInstance()->getPDO()->beginTransaction();
                break;
            case 'commit':
                self::getInstance()->getPDO()->commit();
                break;
            case 'rollback':
                self::getInstance()->getPDO()->rollBack();
                break;
        }
    }

    public function query($sql = NULL, $fnBeforeExecute = NULL)
    {
        if ($sql) {
            $this->setSqlQuery($sql);
        }

        $paginator = $this->getDbPaginatorManager()->getPaginator();
        if ($paginator !== NULL) {
            $paginator->setSqlQuery($this->getSqlQuery());
            $this->setSqlQuery($paginator->getPaginatedQuery());
        }
        $this->queryList[] = $this->getSqlQuery();
        $stmt = $this->getPDO()->prepare($this->getSqlQuery());
        if (is_callable($fnBeforeExecute)) {
            $fnBeforeExecute();
        }

        $stmt->execute();
        return $stmt;
    }

    /**
     * @param DBPaginator $dbp
     */
    public function addDbPaginator($dbp): void
    {
        $dbp->setDBConnection($this);
        DBPaginatorManager::getInstance()->addPaginator($dbp);
    }

    /**
     * @param PDOStatement $stmt
     * @return mixed
     */
    public function fetchObject($stmt)
    {
        return $stmt->fetchObject();
    }

    public function getLastId()
    {
        return $this->getPDO()->lastInsertId();
    }

    /**
     * @param PDOStatement $stmt
     * @return int
     */
    public function numRows($stmt)
    {
        return $stmt->rowCount();
    }

    public function setSqlQuery($sql)
    {
        $this->sqlQuery = $sql;
    }

    public function getSqlQuery()
    {
        return $this->sqlQuery;
    }

    public function escapeString($str)
    {
        return $str;
    }

    public function showErrors($lineBreak = '<br>')
    {
        return implode($lineBreak, $this->getPDO()->errorInfo());
    }

    /**
     * @return PDO
     */
    public function getPDO()
    {
        return $this->_pdoInstance;
    }

    /**
     * @param PDO $i
     */
    public function setPDO($i)
    {
        $this->_pdoInstance = $i;
    }
}
