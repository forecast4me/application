<?php
class SystemNotification {
	public $message, $type;
	public function __construct($message = NULL, $type = 0){
		$this->message = $message;
		$this->type = $type;
	}
	public function getTypeString(){
		switch($this->type){
			case 0: return 'Erro'; break;
			case 1: return "Sucesso"; break;
			case 2: return "Aviso"; break;
			default: return "Notificação"; break;
		}
	}

    public function getIconString()
    {
        switch($this->type){
            case 0: return 'error'; break;
            case 1: return "success"; break;
            case 2: return "warning"; break;
            default: return "info"; break;
        }
    }
}
