<?php
class TextTools {
	public static function capitalize($str){
		return ucfirst($str);
	}
	public static function removeAcentos($texto){
		$array1 = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
		, "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
		$array2 = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
		, "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );
		return str_replace( $array1, $array2, $texto);
	}
	public static function listarArray($itens, $completo = false, $tooltipClass = 'info-tooltip', $hrefTooltip = "javascript:void(0)"){
		$s = sizeof($itens);
		$sf = $s-1;
		$limite = 2;
		foreach($itens as $item){

			if($i){
				if($i==$sf || $i>$limite) $r.=" e ";
				else $r.=", ";
			}

			if(!$completo && $i>$limite){
				$r.= $tooltipClass?"<a href='".$hrefTooltip."' title='".self::listarArray($itens, true)."' class='".$tooltipClass."'>":"";
				$r.= "mais ".($s-$i);
				$r.= $tooltipClass?"</a>":"";
				break;
			}

			$r .= $item;
			array_shift($itens);
			$i++;
		}
		return $r;
	}
	public static function plural($i, $singular, $plural, $nenhum, $printarNumero = true){
		if($i==1){
			return ($printarNumero?$i:"")." $singular";
		}else if($i==0){
			return $nenhum;
		}else{
			return ($printarNumero?$i:"")." $plural";
		}
	}
}