<?php
	class Tools{
		public static function ai($sql, $p = NULL){
			if(is_array($sql)){
				if(sizeof($sql)){
					foreach($sql as $k => $newValue){
						$r[$k] = self::ai($newValue, $p);
					}
					return $r;
				}else{
					return "";
				}
			}

			// remove palavras que contenham sintaxe sql
			$sql = trim($sql);//limpa espaços vazio

			if($p['strip_tags'] != false){
				$sql = strip_tags($sql);//tira tags html e php
			}

			if($p['addslashes'] != false){
				$sql = addslashes($sql);//Adiciona barras invertidas a uma string
			}
			return $sql;
		}

		private static $instance;

		/**
		 * @return Tools
		 */
		public static function getInstance(){
			if (!isset(self::$instance)) {
				$c = __CLASS__;
				self::$instance = new $c;
			}

			return self::$instance;
		}

		public static function redir($url, $useBaseUrl = true){
			ob_clean();

			if($useBaseUrl){
				$url = URL::getBaseURL().$url;
			}
			header("Location: ".$url);
			exit();
		}

		public static function dump($var){
			ob_clean();
			var_dump($var);
			exit();
		}

		public function getClientIP(){
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}

		public function reArrayFiles(&$file_post) {
			$file_ary = array();
			$file_count = count($file_post['name']);
			$file_keys = array_keys($file_post);

			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				}
			}

			return $file_ary;
		}
	}
