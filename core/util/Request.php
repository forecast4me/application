<?php


class Request
{
    private $rawContent;

    /**
     * @return mixed
     */
    public function getRawContent()
    {
        return $this->rawContent;
    }

    /**
     * @param mixed $rawContent
     */
    public function setRawContent($rawContent): void
    {
        $this->rawContent = $rawContent;
    }

    /**
     * @param bool $assoc
     * @return mixed
     */
    public function getContentParsedAsJson($assoc = false)
    {
        return json_decode($this->getRawContent(), $assoc);
    }

    public function getPost($key)
    {
        return Tools::ai($_POST[$key] ?? null);
    }

    public function get($key){
        return Tools::ai($_GET[$key] ?? null);
    }
}
