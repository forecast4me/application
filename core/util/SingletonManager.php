<?php
class SingletonManager {
	private static $instance;
	private function __construct(){}
	/**
	 * @return SingletonManager
	 */
	private static function getInstance(){
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}

	private $_instanceDictionary;

	public function getInstanceDictionary($className){
		return $this->_instanceDictionary[$className];
	}

	public function addInstance($className){
		return $this->_instanceDictionary[$className] = new $className();
	}

	public function removeInstance($className){
		unset($this->_instanceDictionary[$className]);
	}

	public static function getSingleton($className){
		$sm =  SingletonManager::getInstance();

		$instance = $sm->getInstanceDictionary($className);
		if(!$instance) $instance = $sm->addInstance($className);
		return $instance;
	}
} 