<?php

class FileTools
{
    public static function getFileName($fullPath, $removeExtension = false)
    {
        $fullFileName = basename($fullPath);
        if (false === $removeExtension) {
            return $fullFileName;
        }

        return preg_replace('/.[^.]*$/', '', $fullFileName);
    }

    /**
     * @param string $filter
     * @return array
     */
    public static function readDirFiles($filter = "csv/*.csv"): array
    {
        return array_map('self::getFileName', glob($filter));
    }

    /**
     * @param $path
     * @param int $permission
     */
    public static function createDir($path, $permission = 0755): void
    {
        if (!mkdir($path, $permission, true) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }
    }

    /**
     * @param $src
     */
    public static function removeDir($src): void
    {
        $dir = opendir($src);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file !== '.' ) && ( $file !== '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    self::removeDir($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }
}
