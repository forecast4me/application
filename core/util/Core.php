<?php
	class Core{
		private static $instance;
		private function __construct(){}

		/**
		 * @return Core
		 */
		public static function getInstance(){
			if (!isset(self::$instance)) {
				$c = __CLASS__;
				self::$instance = new $c;
			}

			return self::$instance;
		}

		public function load(&$obj, $reg){
			if($reg){
				if(!is_array($reg)) $reg = get_object_vars($reg);
				foreach($reg as $var=>$val){
					if(property_exists($obj, $var)){
						$obj->{$var} = $val;
					}
				}
			}
		}

		//MONTA AUTOMATICAMENTE A QUERY DE INSERT BASEADA EM OBJETO
		public function montarFiltroDeInsersao($i){
			$vars = get_object_vars($i);

			foreach($vars as $var=>$val){
				if($val !== NULL){
					$chaves[] = $var;
					$valores[] = "'".\DB::getInstance()->escapeString($i->{$var})."'";
				}

			}
			return "(".implode(", ", $chaves).") VALUES (".implode(" ,  ", $valores).")";
		}
		public function montarFiltroDeAlteracao($i){
			$vars = get_object_vars($i);
			foreach($vars as $var=>$val){
				if($i->{$var} !== NULL){
					$r[] = $var." = '".DB::getInstance()->escapeString($i->{$var})."'";
				}
			}
			return "SET ".implode(", ", $r);
		}

		public static function getConfig($param){
			$query = "SELECT data FROM config WHERE nome_parametro ='$param'";
			$query = DB::getInstance()->query($query);
			$reg = DB::getInstance()->fetchObject($query);
			return $reg->data;
		}

		public static function checkServerFeature($featureName){
			if(function_exists('apache_get_modules') && in_array($featureName, apache_get_modules())){
				return true;
			}
			return false;
		}
	}