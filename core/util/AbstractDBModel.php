<?php
	abstract class AbstractDBModel {
		protected $_tableName, $_primaryKey, $_orderField, $_statusField;

		/**
		 * @param null $filtroSql
		 * @param string $fields
		 * @return \MysqlIterator | $this
		 */
		public function search($filtroSql = NULL, $fields = "*"){
			$sql = "SELECT ".$fields." FROM ".$this->_tableName." ".$filtroSql;
			$query = \DB::getInstance()->query($sql);
			$c = get_called_class();
			return new \MysqlIterator($query, new $c());
		}

		/**
		 * @param $id
		 * @return $this
		 */
		public function searchById($id){
			return $this->searchByIds($id)->getFirst();
		}

		/**
		 * @param $ids
		 * @return \MysqlIterator | $this[]
		 */
		public function searchByIds($ids){
			if(is_array($ids)){
				$ids = implode(", ", $ids);
			}

			return $this->search("WHERE ".$this->_primaryKey." IN (".$ids.")");
		}

		/**
		 * @param $post
		 * @param $files
		 * @return bool
		 */
		public function add($post = NULL, $files = NULL){
			if($this->_orderField){
				if(!$this->{$this->_orderField} === NULL){
					$this->{$this->_orderField} = $this->getNextOrderPosition();
				}
			}

			if($this->_statusField){
				if($this->{$this->_statusField} === NULL){
					$this->{$this->_statusField} = 1;
				}
			}

			$insertSql = \Core::getInstance()->montarFiltroDeInsersao($this);
			$sql = "INSERT INTO ".$this->_tableName." ".$insertSql;
			if(!\DB::getInstance()->query($sql)){
				\Core::log("Inserting element fail: \n Query: ".\DB::getInstance()->getSqlQuery(). "\n Error: ".\DB::getInstance()->showErrors('\\'));
				return false;
			}

			return $this->{$this->_primaryKey} = \DB::getInstance()->getLastId();
		}

		/**
		 * @return bool
		 */
		public function edit(){
			$alterarSql = \Core::getInstance()->montarFiltroDeAlteracao($this);
			$sql = "UPDATE ".$this->_tableName." ".$alterarSql." WHERE ".$this->_primaryKey." = ".$this->{$this->_primaryKey};
			if(!\DB::getInstance()->query($sql)){
				\Core::log("Editing element fail: \n Query: ".\DB::getInstance()->getSqlQuery(). "\n Error: ".\DB::getInstance()->showErrors('\\'));
				return false;
			}
			return true;
		}

		/**
		 * @return bool
		 */
		public function delete(){
			if($this->_statusField){
				$this->{$this->_statusField} = 0;
				return $this->edit();
			}else{
				$sql = "DELETE FROM ".$this->_tableName." WHERE ".$this->_primaryKey." = '".$this->{$this->_primaryKey}."'";
				if(!DB::getInstance()->query($sql)){
					\Core::log("Deleting element fail: \n Query: ".\DB::getInstance()->getSqlQuery(). "\n Error: ".\DB::getInstance()->showErrors('\\'));
					return false;
				}
				return true;
			}
		}

		/**
		 * @return AbstractDBModel
		 */
		private function getLastInsertedElement(){
			$listaElementos = $this->search(($this->_statusField?"WHERE ".$this->_statusField." = 1 ":"")."ORDER BY ".$this->_primaryKey." DESC");
			if(!$listaElementos){
				return NULL;
			}
			return $listaElementos->getFirst();
		}

		public function getNextOrderPosition(){
			if(!$this->_orderField){
				return 0;
			}

			$ultimoElemento = $this->getLastInsertedElement();
			if(!$ultimoElemento->{$this->_primaryKey}){
				return 1;
			}
			return $this->getLastInsertedElement()->{$this->_orderField}+1;
		}

		protected function _setupDBModel($tableName, $primaryKey, $statusField = null, $orderField = null){
			$this->_tableName = $tableName;
			$this->_primaryKey = $primaryKey;
			$this->_statusField = $statusField;
			$this->_orderField = $orderField;
		}
	}