<?php
class FileOutput implements IOutput {
	public $vars;

	/**
	 * @param array $vars
	 */
	public function setVars($vars) {
		$this->vars = $vars;
	}

	/**
	 * @return array
	 */
	public function getVars() {
		return $this->vars;
	}

	public function addVar($k, $v){
		$this->vars[$k] = $v;
	}

	public function removeVar($k){
		unset($this->vars[$k]);
	}

	public function __construct($vars = null){
		$this->setVars($vars);
	}

    /**
     * @return string
     */
	public function render(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Content-Type, Content-Disposition");
        header('Content-Disposition: attachment; filename=' . ($this->vars['filename'] ?? 'file.raw'));
        header('Content-Type: application/octet-stream');
        ob_clean();

        return $this->vars['content'];
	}

	public function __toString(){
		return $this->render();
	}
}
