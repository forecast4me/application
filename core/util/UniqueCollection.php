<?php
	class UniqueCollection {
		var $mainAttr;
		var $titleAttr;
		var $arrId;
		var $arrObj;
		private $_elementCount;

		public function __construct($mainAttr, $titleAttr = NULL){
			$this->mainAttr = $mainAttr;
			$this->titleAttr = $titleAttr;
		}
		public function buscarPorId($id){
			return $this->arrObj[$id];
		}
		public function adicionar($el){
			$this->arrObj[$el->{$this->mainAttr}] = $el;
			$this->plusSize();
		}
		public function adicionarArr($arrObj){
			if(sizeof($arrObj)){
				foreach($arrObj as $obj){
					$this->adicionar($obj);
				}
			}
		}
		public function remover($el){
			$this->arrObj[$el->{$this->mainAttr}] = NULL;
			unset($arrObj[$el->{$this->mainAttr}]);
			$this->arrObj = array_filter($this->arrObj);
			$this->minusSize();
		}
		public function removerArr($arrObj){
			if(sizeof($arrObj)){
				foreach($arrObj as $obj){
					$this->remover($obj);
				}
			}
		}
		public function __toString(){
			if(!$this->titleAttr) {
				return "";
			}

			if(!sizeof($this->arrObj)) {
				return "Lista de objetos vazia";
			}

			foreach($this->arrObj as $obj){
				$r.= $obj->{$this->titleAttr}."<br>";
			}
			return $r;

		}
		public function getSize(){
			return $this->_elementCount;
		}
		private function minusSize(){
			if($this->_elementCount > 0){
				$this->_elementCount--;
			}
		}
		private function plusSize(){
			$this->_elementCount++;
		}
	}