<?php
	class MysqlIterator implements Iterator, Countable, ArrayAccess{
		/**
		 * @var PDOStatement $_preparedStatement
		 */
		private $_preparedStatement;
		private $_currentElement;
		private $_position;
		private $_instanceType;
		private $_array;

		/**
		 * @param PDOStatement $preparedStatement
		 * @param $i
		 */
		public function __construct($preparedStatement, $i){
			$this->_preparedStatement = $preparedStatement;
			$this->_position = 0;
			$this->_instanceType = get_class($i);
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Return the current element
		 * @link http://php.net/manual/en/iterator.current.php
		 * @return mixed Can return any type.
		 */
		public function current()
		{
			if($this->getArray(false)){
				return $this->getArray()[$this->_position];
			}
			$dbRes = DB::getInstance()->fetchObject($this->_preparedStatement);
			$item = new $this->_instanceType();
			Core::getInstance()->load($item, $dbRes);
			$this->_currentElement = $item;
			return $this->_currentElement;
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Move forward to next element
		 * @link http://php.net/manual/en/iterator.next.php
		 * @return void Any returned value is ignored.
		 */
		public function next()
		{
			$this->_position++;
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Return the key of the current element
		 * @link http://php.net/manual/en/iterator.key.php
		 * @return mixed scalar on success, or null on failure.
		 */
		public function key()
		{
			return $this->_position;
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Checks if current position is valid
		 * @link http://php.net/manual/en/iterator.valid.php
		 * @return boolean The return value will be casted to boolean and then evaluated.
		 * Returns true on success or false on failure.
		 */
		public function valid()
		{
			return ($this->_position < $this->count());
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Rewind the Iterator to the first element
		 * @link http://php.net/manual/en/iterator.rewind.php
		 * @return void Any returned value is ignored.
		 */
		public function rewind()
		{
			$this->_position  = 0;
		}

		/**
		 * (PHP 5 &gt;= 5.1.0)<br/>
		 * Count elements of an object
		 * @link http://php.net/manual/en/countable.count.php
		 * @return int The custom count as an integer.
		 * </p>
		 * <p>
		 * The return value is cast to an integer.
		 */
		public function count()
		{
			return DB::getInstance()->numRows($this->_preparedStatement);
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Whether a offset exists
		 * @link http://php.net/manual/en/arrayaccess.offsetexists.php
		 * @param mixed $offset <p>
		 * An offset to check for.
		 * </p>
		 * @return boolean true on success or false on failure.
		 * </p>
		 * <p>
		 * The return value will be casted to boolean if non-boolean was returned.
		 */
		public function offsetExists($offset)
		{
			return $this->_currentElement;
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Offset to retrieve
		 * @link http://php.net/manual/en/arrayaccess.offsetget.php
		 * @param mixed $offset <p>
		 * The offset to retrieve.
		 * </p>
		 * @return mixed Can return all value types.
		 */
		public function offsetGet($offset)
		{
			return $this->current();
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Offset to set
		 * @link http://php.net/manual/en/arrayaccess.offsetset.php
		 * @param mixed $offset <p>
		 * The offset to assign the value to.
		 * </p>
		 * @param mixed $value <p>
		 * The value to set.
		 * </p>
		 * @return void
		 */
		public function offsetSet($offset, $value)
		{
			$this->_array[$offset] = $value;
		}

		/**
		 * (PHP 5 &gt;= 5.0.0)<br/>
		 * Offset to unset
		 * @link http://php.net/manual/en/arrayaccess.offsetunset.php
		 * @param mixed $offset <p>
		 * The offset to unset.
		 * </p>
		 * @return void
		 */
		public function offsetUnset($offset)
		{
			unset($this->_array[$offset]);
		}

		/**
		 * @return mixed
		 */
		public function getFirst(){
			return $this->current();
		}

		private function buscarArray(){
			$arr = array();

			$this->rewind();
			while($this->valid()){
				$arr[] = $this->current();
				$this->next();
			}
			return $arr;
		}

		private function getArray($load = true){
			if(!$this->_array && $load){
				$this->setArray($this->buscarArray());
			}
			return $this->_array;
		}

		private function setArray($arr){
			$this->_array = $arr;
		}

		public function toArray(){
			return $this->getArray(true);
		}

		public function has($element){
			return in_array($element, $this->toArray());
		}
	}