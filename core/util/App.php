<?php
class App {
	public static function checkServerFeatures(){
		if(!Core::checkServerFeature('mod_rewrite')){
			die("Por favor, habilite o mod_rewrite para executar este exemplo.");
		}
	}
	public function run(){
		self::checkServerFeatures();

		//FAZ O MAPEAMENTO DA URL
		URL::map();
		URL::set("Area", str_replace("-", "", URL::get("Area")));
		URL::set("action", str_replace("-", "", URL::get('action')));

		if(!URL::get("Area")){
			Tools::redir(URL::getURL(["Area"=>"Home"]), false);
		}

		//DEFINE AS PRINCIPAIS CONSTANTES
		define("AREA", Tools::getInstance()->ai(URL::get('Area')));
		define("ACTION", URL::get("action")?URL::get("action"):"index");
		define("ID", Tools::getInstance()->ai(URL::get('ID')));

		$AREA = AREA;
		switch($_SERVER['REQUEST_METHOD']){
			case "POST":
				$ACTION = URL::get("action")?"post".TextTools::capitalize(URL::get("action")):"postIndex";
				break;
            case "OPTIONS":
                echo (new JsonOutput([]))->render();
                exit();
                break;
			default:
				$ACTION = URL::get("action")?URL::get("action"):"index";
		}

		session_start();

        /**
         * @var Request $request
         */
		$request = SingletonManager::getSingleton(Request::class);
		$request->setRawContent(file_get_contents('php://input'));

		//CARREGAMENTO DO CONTROLLER
		$controllerName = $AREA."Controller";
		if(file_exists("lib/controller/".$AREA."/".$controllerName.".php")){
			include("lib/controller/".$AREA."/".$controllerName.".php");
			$controller = new $controllerName();
		}

		$actionName = $ACTION."Action";
		if(method_exists($controller, $actionName)){
			$result = $controller->$actionName();
		}else{
			$result = new HtmlOutput('Error/404');
		}
        /**
         * @var IOutput $result
         */
		echo $result;
		exit();
	}
}
