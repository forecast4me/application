<?php


class AbstractSingleton
{
    private static $uniqueInstance = null;

    public static function getInstance(): ?\AbstractSingleton
    {
        if (self::$uniqueInstance === null) {
            $c = static::class;
            self::$uniqueInstance = new $c();
        }

        return self::$uniqueInstance;
    }

    protected function __construct()
    {
    }

    private function __clone()
    {
    }
}
