<?php
//AUTOLOADER
spl_autoload_register(
	function($className){
		//PSR-0
		$className = ltrim($className, '\\');
		$fileName  = '';
		$namespace = '';
		if ($lastNsPos = strrpos($className, '\\')) {
			$namespace = substr($className, 0, $lastNsPos);
			$className = substr($className, $lastNsPos + 1);
			$fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
		}
		$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

		if(file_exists("core/util/".$fileName)){
			require "core/util/".$fileName;
		}else if(file_exists("lib/model/".$fileName)){
            require "lib/model/".$fileName;
		}else if(file_exists("lib/service/".$fileName)){
            require "lib/service/".$fileName;
        }else if(file_exists("lib/repository/".$fileName)){
            require "lib/repository/".$fileName;
        }
	}
);

require_once("vendor/autoload.php");
