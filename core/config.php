<?php
    URL::getInstance()->setBaseURL(getenv('BASE_URL'));
    define("ENABLE_CACHE", getenv('ENABLE_CACHE'));

	//DEBUG
	if(getenv('DEBUG')){
        ini_set('display_errors', true);
        error_reporting(E_ALL^E_NOTICE^E_WARNING);
    }

	//DB
	$db = DB::getInstance();
	$db->host	= getenv('DB_HOST');
	$db->db		= getenv('DB_NAME');
	$db->user 	= getenv('DB_USER');
	$db->pass	= getenv('DB_PASS');

	//UPLOAD DE ARQUIVOS
	ini_set("upload_max_filesize", getenv('MAX_FILE_UPLOAD'));
	set_time_limit(getenv('MAX_PROCESS_TIMEOUT'));
    ini_set('memory_limit', "-1");

	//TIMEZONE
	date_default_timezone_set(getenv("TIMEZONE"));

	if(!DB::connect()){
	    echo SystemNotificator::getInstance();
	    exit();
    }
